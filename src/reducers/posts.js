
const INITIAL_STATE = {
    posts: [],
    categories: [],
    categoryDetail: [],
    programs: [],
    history: [],
    training: [],
    spacialPosts: [],
    headerCategories: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'USER_POSTS_FETCHED':
            return { ...state, posts: action.payload };
        case 'SPACIAL_POSTS_FETCHED':
            return { ...state, spacialPosts: action.payload };
        case 'CATEGORIES_POSTS_FETCHED':
            return { ...state, headerCategories: action.payload };
            case 'CATEGORY_POSTS_FETCHED':
            return { ...state, categories: action.payload };
        case 'NEW_POSTS_FETCHED':
            const newArray = [...state.posts, ...action.payload];
            // const newArray = state.posts.data.concat(action.payload.data);
            let newPosts = state.posts;
            // newPosts = newArray;
            return {
                ...state,
                posts: newArray,
            };
            case 'NEW_CATEGORIES_FETCHED':
                console.log('payload', ...action.payload)
            const newArray2 = [...state.categories, ...action.payload];
                console.log('new payload', newArray2)
            return {
                ...state,
                categories: newArray2,
            };
        case 'DETAIL_POST_FETCHED':
            return { ...state, categoryDetail: action.payload };
        default:
            return state;
    }
};
