import {createStore, applyMiddleware} from 'redux';
import logger from "redux-logger";
import thunk from "redux-thunk";
import {composeWithDevTools} from 'redux-devtools-extension';
import  AppReducers from '../reducers';

export const store = createStore(AppReducers, {}, composeWithDevTools(
    applyMiddleware(
        thunk,
        logger
    )
));
