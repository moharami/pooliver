import React, {Component} from 'react';
import {Text, View, ScrollView, Image, TouchableOpacity} from 'react-native';
import styles from './styles'
import 'moment/locale/fa';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {Actions} from 'react-native-router-flux'
import moment_jalaali from 'moment-jalaali'
import LinearGradient from 'react-native-linear-gradient'

class Multimedia extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={() => Actions.blogDetail({item: this.props.item})}>
                {
                    this.props.item.attachments[0] !== undefined ?
                        <Image source={{uri: 'https://pooliver.azarinpro.info/files?uid='+this.props.item.attachments[0].uid+'&width=350&height=350'}} style={styles.image} />
                        : null
                }
                <LinearGradient
                    start={{x: 1, y: 0}} end={{x: 1, y: 1}} colors={[ 'rgb(224, 220, 216)', 'white']} style={styles.content}>
                {/*<View style={styles.content}>*/}
                    <Text style={styles.contentText}>{this.props.item.title.substr(0, 20) }.... </Text>
                    <View style={styles.advRow}>
                        <View style={styles.iconContainer}>
                            {/*<Icon name="heart" size={20} color="red" style={{paddingLeft: 10}} />*/}
                            {/*<Icon name="bookmark-o" size={20} color="black" style={{paddingLeft: 25}} />*/}
                        </View>
                        <View style={styles.labelContainer}>
                            <Text style={styles.bodyText}>250 امتیاز</Text>
                            <Text style={styles.footerText}>مدت زمان 3 دقیقه</Text>
                        </View>
                    </View>
                {/*</View>*/}
                </LinearGradient>
            </TouchableOpacity>
        );
    }
}
export default Multimedia;