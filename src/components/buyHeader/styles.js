import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingRight: 15,
        paddingLeft: 15,
        paddingTop: 20

    },
    navContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
        // width: 84,
        // paddingBottom: 40,
        paddingLeft: 25

    },
    image: {
        width: 48,
        resizeMode: 'contain'
    },
    numContainer: {
        width: 30,
        height: 30,
        alignItems: "center",
        justifyContent: 'center',
        padding: 10,
        backgroundColor: 'red',
        borderRadius: 80
    },
    text: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',

    },
    label: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 7,
        color: 'rgba(216, 154, 0, 1)'
    },
    seprator: {
        paddingRight: 10,
        paddingLeft: 10
    },
});