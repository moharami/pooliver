import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        // backgroundColor: 'white',
        height: 120,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,

        // paddingTop: 50,
        // paddingBottom: 100,

    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        paddingTop: 10,
        backgroundColor: 'white',
        width: '100%',
        paddingBottom: 10,
        paddingRight: 5,
        paddingLeft: 5,

    },
    headerTitle: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingRight: '34%'
    },
    name: {
        color: 'gray',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
    }
});