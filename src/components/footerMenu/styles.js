
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-around',
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 9999,
        height: 80,
        borderTopWidth: 1,
        borderTopColor: 'rgb(237, 237, 237)'
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        // paddingTop: 6,
        paddingBottom: 25,
        height: 80,


    }
});