import React, { Component } from 'react';
import {
    Modal,
    View,
    StyleSheet
}
    from 'react-native'
import Axios from 'axios';
export const url = 'http://bimecoffe.azarinpro.info/api/v1';
Axios.defaults.baseURL = url;

import {
    BarIndicator,
} from 'react-native-indicators';

class AlertModal extends Component {
    state = {
        modalVisible: false,
        value: '',
        lineAmount: 1,
        text: ''
    };
    render() {
        return (
            <View style = {styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style={styles.modal}>
                        <View style={{width: '100%', height: 60}}>
                            <BarIndicator color='white' size={50} style={{height: 90}} />
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default AlertModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding: 100,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.5)',
        paddingRight: 15,
        paddingLeft: 15
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '70%',
        height: 60,
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        borderRadius: 10

    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    vacleContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    }
});