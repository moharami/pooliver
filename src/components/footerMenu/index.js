import React from 'react';
import {Text, View, TouchableOpacity, AsyncStorage, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Axios from 'axios'
import AlertModal from './alertModal';
export const url = 'http://172.20.21.25:8009/api/v1';
Axios.defaults.baseURL = url;
import magnify from '../../assets/search.png'

export default class FooterMenu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0,
            loading: false,
            modalVisible: false
        };
    }
    test(item) {
        console.log('herre is footer menu')
        this.setState({modalVisible: true});
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                console.log('comment token', newInfo.token);
                const expiresTime = newInfo.expires_at;
                const currentTime = new Date().getTime()/1000;
                if(expiresTime> currentTime ){
                    // Actions.home({openDrawer: this.props.openDrawer, loged: true})
                    if(item === 'profile'){
                        Actions.profile({openDrawer: this.props.openDrawer})
                    }
                    else if(item === 'remind'){
                        Actions.reminders({openDrawer: this.props.openDrawer})
                    }
                    this.setState({modalVisible: false});
                }
                else {
                    Actions.login({openDrawer: this.props.openDrawer, profile: true})
                    this.setState({modalVisible: false});
                    // this.setState({info: true});
                }
            }
            else{
                Actions.login({openDrawer: this.props.openDrawer, profile: true});
                this.setState({modalVisible: false});
            }
        });
    }
    render(){
        // if(this.state.loading){
        //     return (<Loader />)
        // }
        // else
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => Actions.insuranceBuy()} style={{width: '20%'}}>
                    <View style={styles.navContainer}>
                        <SIcon name="home" size={25} color={this.props.active === 'bime' ? 'rgb(0, 121, 255)': 'gray'} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() =>  Actions.home({openDrawer: this.props.openDrawer})} style={{width: '20%'}}>
                    <View style={styles.navContainer}>
                        <Image source={magnify} style={{width: 27, height: 27, resizeMode: 'contain', tintColor: 'rgb(70, 70, 70)'}} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.test('profile')} style={{width: '20%'}}>
                    <View style={styles.navContainer}>
                        <FIcon name="user" size={25} color={this.props.active === 'profile' ? 'rgb(0, 121, 255)': 'gray'} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.test('remind')} style={{width: '20%'}}>
                    <View style={styles.navContainer}>
                        <FIcon name="info" size={25} color={this.props.active === 'remind' ? 'rgb(0, 121, 255)': 'gray'} />
                    </View>
                </TouchableOpacity>
                <AlertModal
                    closeModal={(title) => this.closeModal(title)}
                    onChange={(visible) => this.setState({modalVisible: false})}
                    modalVisible={this.state.modalVisible}
                    modelCats={this.state.modelCats}
                />
            </View>
        );
    }
}