import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    topContainer: {
        width: '100%',
        // height: '100%',
        borderRadius: 10,
        marginBottom: 30,
        elevation: 4
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        borderRadius: 10,
        elevation: 4
    },
    header: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        // borderBottomColor: 'rgb(237, 237, 237)',
        // borderBottomWidth: 1,
        paddingLeft: 10,
        paddingBottom: 10
    },
    info: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        padding: 10
    },
    price: {
        flexDirection: 'row'
    },
    priceLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        paddingRight: 5
    },
    labelInfo: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(50, 50, 50)'

    },
    amount: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    Image: {
        width: 50,
        resizeMode: 'contain',
        height: 50,
        marginTop: 15,
        marginRight: 10
    },
    left: {
        flexDirection: 'row',
        // width: '70%',
    },
    right: {
        // flexDirection: 'row',
    },
    rightContainer: {
        // flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 3,
        paddingLeft: 3,
        // borderRadius: 20,
        backgroundColor: 'rgba(122, 130, 153, 1)',
        // overflow: 'hidden'
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopRightRadius: 10,
        // overflow: 'hidden',

    },
    label: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'white',
        paddingLeft: 10,
        paddingRight: 10
    },
    label2: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        color: 'white',
        paddingLeft: 10,
        paddingRight: 10
    },
    value: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        fontSize: 14
    },
    redValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'red',
        fontSize: 12
    },
    regimContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 8,
    },
    body: {
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 15,
        paddingBottom: 15,
        borderTopColor: 'rgb(237, 237, 237)',
        borderTopWidth: 1,
        position: 'relative',
        zIndex: 60
    },
    bodyLeft: {
        flexDirection: 'row',
        paddingLeft: 30
    },
    bodyRight: {
        flexDirection: 'row'
    },
    bodyLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10
    },
    bodyValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 10,
        paddingLeft: 3,
        color: 'black'
    },
    instalmentLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        color: 'black'
    },
    instalmentValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        lineHeight: 20,
        fontSize: 11
    },
    row: {
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 5,
        paddingBottom: 5,
        paddingRight: 20,
        paddingLeft: 20
    },
    image: {
        width: 40,
        height: 40,
        resizeMode: 'contain',
        marginTop: 5
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(255, 45, 85, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center',
    },
    iconRightContainer: {
        backgroundColor: 'rgba(51, 197, 117, 1)',
        height: 35,
        paddingLeft: 3,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginTop: 10
    },
    btnContainer: {
        // width: '35%',
        backgroundColor: 'rgba(51, 197, 117, 1)',
        height: 32,
        paddingLeft: 5,
        paddingRight: 5,
        borderRadius: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10
    },
    footer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        backgroundColor: 'rgba(245, 246, 250, 1)',
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderTopColor: 'rgb(237, 237, 237)',
        borderTopWidth: 1,

    },
    startContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    iconRightEditContainer: {
        backgroundColor: 'rgba(51, 197, 117, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10
    },
    describeContainer: {
        paddingRight: 5,
        paddingLeft: 5,
        backgroundColor: 'rgb(0, 150, 136)',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        marginRight: 5
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
    }
});
