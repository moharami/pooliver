import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        borderRadius: 10,
        // backgroundColor: 'white',
        // paddingRight: 10,
        // paddingLeft: 10,
        // paddingBottom: 1
    },
    left: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
    },
    labelContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        paddingBottom: 20
    },
    label: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 10
    }
});