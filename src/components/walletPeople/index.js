import React, {Component} from 'react';
import {View, Text, AsyncStorage, Alert} from 'react-native';
import styles from './styles'
import PeopleItem from '../../components/peopleItem'
import Icon from 'react-native-vector-icons/Feather'

class WalletPeople extends Component {
    constructor(props){
        super(props);
    }
    render() {
       return (
            <View style={styles.container}>
                {/*<View style={styles.labelContainer}>*/}
                    {/*<Text style={styles.label}>افراد دعوت شده </Text>*/}
                    {/*<Icon name="user" size={20} color="gray" />*/}
                {/*</View>*/}
                    {
                        this.props.peoples.length !== 0 ?
                            <View style={{backgroundColor: 'white', borderRadius: 10}}>
                                {
                                    this.props.peoples.map((item,index)=> <PeopleItem border item={item} key={index} />)

                                }
                            </View>
                             : <Text style={{color: 'white', textAlign: 'center'}}>لیست افراد دعوت شده خالی می باشد</Text>
                    }
                {/*</View>*/}
            </View>
        );
    }
}
export default WalletPeople;