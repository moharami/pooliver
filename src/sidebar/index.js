import React, {Component} from 'react';
import {Text, View,Dimensions, ScrollView, AsyncStorage, Alert, TouchableOpacity} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'

import Axios from 'axios';
// export const url = 'http://172.20.21.25:8009/api/v1';
// Axios.defaults.baseURL = url;
import SidebarItem from '../components/sidebarItem'
import Loader from '../components/loader'
import AlertView from '../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient'
import {connect} from 'react-redux';

class Sidebar extends React.Component {
    constructor(){
        super();
        this.state = {
            loading: false,
            user: {},
            show: false,
            modalVisible: false,
            request: false,
            showMore : false,
            login: false
        };
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount() {
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {

                const newInfo = JSON.parse(info);
                const expiresTime = newInfo.expires_at;
                const currentTime = new Date().getTime()/1000;
                console.log('expiresTime', expiresTime);
                console.log('currentTime', currentTime);
                if(expiresTime> currentTime){
                    Axios.post('/request/market/get-status', {
                        user_id: newInfo.user_id
                    }).then(response => {
                        this.setState({loading: false, statuses: response.data.data, showMore: true});
                        console.log('statuses',response.data.data)
                        if(response.data.data === "") {
                            this.setState({request: false, showMore: true});
                        }
                        else if(response.data.data !== "" && response.data.data.status === "request") {
                            this.setState({request: false, showMore: true});
                        }
                        else if(response.data.data !== "" && response.data.data.status === "approve") {
                            this.setState({request: true, showMore: true});
                        }
                        else if(response.data.data !== "" && response.data.data.status === "reject") {
                            this.setState({request: false, showMore: true});
                        }
                        const info = {'showMore': true};
                        const newwwwAsync = AsyncStorage.setItem('showMore', JSON.stringify(info));
                        console.log('newwwwAsync', newwwwAsync);
                        AsyncStorage.getItem('showMore').then((info) => {
                            if(info !== null) {
                                const newInfo = JSON.parse(info);
                                console.log('showMore', newInfo)
                                newInfo.showMore === true ? this.setState({showMore: true}) :  this.setState({showMore: false});
                            }
                        })
                    })
                    .catch((error) =>{
                        this.setState({modalVisible: true, loading: false});
                    });
                }
                else {
                    this.setState({showMore : false, loading: false});
                }
            }
            else{
                this.setState({showMore : false, loading: false});
            }
        });
    }
    // componentDidUpdate() {
    //     let loged2 = null;
    //     AsyncStorage.getItem('loged').then((info) => {
    //         if(info !== null) {
    //             const newInfo = JSON.parse(info);
    //             console.log('new info sidebar', newInfo)
    //             newInfo.loged === true ? this.setState({login: true}) : null;
    //         }
    //     })
    //     console.log('loged side bar var', this.state.login)
    //     console.log('showMore', this.state.showMore)
    // }

    componentWillReceiveProps() {
        this.forceUpdate();

        let loged2 = null;
            AsyncStorage.getItem('loged').then((info) => {
                if(info !== null) {
                    const newInfo = JSON.parse(info);
                    newInfo.loged === true ? this.setState({login: true}) : null;
                }
            })
        AsyncStorage.getItem('showMore').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                newInfo.showMore === true ? this.setState({showMore: true}) : this.setState({showMore: false});
            }
        })

    }
    userLogOut() {
        this.setState({login: false})
        AsyncStorage.removeItem('loged');
    }
    render(){
        // const {loged} = this.props;

        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <LinearGradient
                style={{height: Dimensions.get('window').height}}
                start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']}>
            <ScrollView style={styles.scroll}>
                <View style={styles.container}>
                    <View style={styles.profile}>
                        {/*<Text style={styles.name}>{this.state.user.fname} {this.state.user.lname}</Text>*/}
                        <View style={styles.iconContainer}>
                            {/*{this.state.user.attachments !== undefined ?*/}
                                {/*<Image source={{uri: 'http://http://172.20.21.25:8009/api/v1/files?uid='+this.state.user.attachments.uid+'&width=350&height=350'}} style={styles.image} />*/}
                                {/*: null*/}
                            {/*}*/}
                        </View>
                    </View>
                    {/*<SidebarItem label="خانه" icon="home" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>*/}
                    <SidebarItem label="خرید بیمه" icon="shield" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                    {this.state.login ?
                        <View>
                            <SidebarItem label="کیف پول من" icon="md-wallet" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                            <SidebarItem label="سفارش های من" icon="receipt" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                            <SidebarItem label="یادآوری ها" icon="event-note" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                            {/*<SidebarItem label="معرفی به دوستان" icon="users" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>*/}
                            {
                                this.state.request ?
                                    <View>
                                        <SidebarItem label="لینک دعوت" icon="wallet" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                                        <SidebarItem label="دعوت شده ها" icon="users" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                                    </View>
                                    :
                                    <SidebarItem label="درخواست بازاریابی" icon="wallet" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                            }
                            <SidebarItem label="بیمت  چیست؟" icon="question-circle" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                            <SidebarItem label="قوانین و مقررات" icon="balance-scale" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>
                        </View> : null
                    }
                    {/*<SidebarItem label="تنظیمات" icon="md-settings" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer}/>*/}
                    <SidebarItem label="خروج" icon="logout" openDrawer={this.props.openDrawer} closeDrawer={this.props.closeDrawer} userLogOut={() => this.userLogOut()} />
                    <AlertView
                        closeModal={(title) => this.closeModal(title)}
                        // onChange={() => this.setState({modalVisible: false})}
                        modalVisible={this.state.modalVisible}
                        title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید '
                    />
                </View>
            </ScrollView>
            </LinearGradient>
        );
    }
}

export default Sidebar;