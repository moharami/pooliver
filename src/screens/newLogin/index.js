import React, {Component} from 'react';
import {BackHandler, AsyncStorage, ImageBackground, View, Text, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import Axios from 'axios'
export const url = 'http://172.20.21.25:8009/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import {store} from '../../config/store';
import moment_jalaali from 'moment-jalaali'
import img from '../../assets/homeBG2.png'
import Icon from 'react-native-vector-icons/Feather';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import TimerCountdown from 'react-native-timer-countdown';

class NewLogin extends Component {
    constructor(props) {
        super(props);
        this.backCount = 0;
        this.state = {
            mobile: '',
            code: '',
            codeRecive: true
        };
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress",  this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <ImageBackground style={styles.image} source={img}>
                <Text style={[styles.label, { paddingBottom: this.state.codeRecive ? 20 : 50, paddingTop: this.state.codeRecive ? 20 : 0}]}>برای ورود شماره موبایل خود را وارد نمایید. </Text>
                <View style={[styles.telContainer, {marginBottom: 20}]}>
                    <Text style={[styles.telLabel, {fontSize: 13}]}>+98</Text>
                    <TextInput
                        // maxLength={1}
                        maxLength={11}
                        autoFocus={!this.state.codeRecive}
                        keyboardType='numeric'
                        placeholderTextColor={'gray'}
                        underlineColorAndroid='transparent'
                        style={{
                            textAlign: 'left',
                            height: 20,
                            // backgroundColor: 'red',
                            fontSize: 12,
                            // color: '#7A8299',
                            color: 'black',
                            width: 130,
                            fontFamily: 'IRANSansMobile(FaNum)',
                            padding: 0
                        }}
                        value={this.state.mobile}
                        onChangeText={(text) => this.setState({mobile: text})}/>
                    <Text style={styles.telLabel}>شماره همراه </Text>
                    <Icon name="phone-call" size={14} color={'gray'} style={styles.icon} />
                </View>
                {
                    this.state.codeRecive ?
                        <View style={styles.telContainer}>
                            <TextInput
                                // maxLength={1}
                                maxLength={11}
                                autoFocus={this.state.codeRecive}
                                keyboardType='numeric'
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                style={{
                                    textAlign: 'left',
                                    height: 20,
                                    // backgroundColor: 'red',
                                    fontSize: 12,
                                    // color: '#7A8299',
                                    color: 'black',
                                    width: 130,
                                    fontFamily: 'IRANSansMobile(FaNum)',
                                    padding: 0,
                                }}
                                value={this.state.code}
                                onChangeText={(text) => this.setState({code: text})}/>
                            <View style={{flexDirection: 'row'}}>
                                <Text style={[styles.telLabel, {alignSelf: 'flex-end'}]}>کد</Text>
                                <MIcon name="cellphone-message" size={14} color={'gray'} style={{padding: 2}}/>
                            </View>
                        </View> : null
                }
                {
                    this.state.codeRecive ?
                        <View style={styles.footer}>
                            <TimerCountdown
                                initialSecondsRemaining={1000*60}
                                // onTimeElapsed={() => this.timerComplete()}
                                onTimeElapsed={() => null}
                                allowFontScaling={true}
                                style={{ fontSize: 18, paddingTop: 15,  fontFamily: 'IRANSansMobile(FaNum)'}}
                            />
                            <Text style={styles.reSend}>ارسال دوباره کد</Text>
                        </View>
                        : null
                }

                <View style={styles.loginButton}>
                    <Icon name="arrow-left" size={14} color={'white'} />
                    <Text style={styles.btnLabel}>ورود</Text>
                </View>
            </ImageBackground>
        );
    }
}

export default NewLogin;