import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'rgb(244, 244, 244)'
    },
    image: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingRight: 15
    },
    label: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 13
    },
    telLabel: {
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 11,
        paddingRight: 5
    },
    telContainer: {
        flexDirection: 'row',
        alignItems: "flex-end",
        justifyContent: 'space-between',
        borderBottomColor: 'rgb(172, 172, 172)',
        borderBottomWidth: 3,
        width: '70%'
    },
    icon: {
        borderRadius: 100,
        borderWidth: 1,
        borderColor: 'gray',
        padding: 2
    },
    loginButton: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-around',
        paddingRight: 40,
        paddingLeft: 40,
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: 'rgb(217, 0, 0)',
        borderRadius: 30,
        position: 'absolute',
        bottom: '20%',
        left: '20%',
        zIndex: 90
    },
    btnLabel: {
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15
    },
    reSend: {
        fontSize: 10,
        paddingTop: 20,
        color: 'rgb(50, 50, 50)',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    footer: {
        flexDirection: 'row',
        alignItems: "flex-end",
        justifyContent: 'space-between',
        width: '70%'
    }

});
