import React, {Component} from 'react';
import { View, ImageBackground, Text, BackHandler, Dimensions, ScrollView, Image, TouchableOpacity, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import Axios from 'axios';
export const url = 'http://172.20.21.25:8009/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import ProfileItem from "../../components/profileItem/index";
import FIcon from 'react-native-vector-icons/dist/Feather';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import IIcon from 'react-native-vector-icons/dist/Ionicons';
import FooterMenu from "../../components/footerMenu/index";
import {store} from '../../config/store';
import {connect} from 'react-redux';
import AlertView from '../../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient'
import hd from '../../assets/hd3.png'
import pr from '../../assets/prize.png'
import us from '../../assets/users.png'
import PersianCalendarPicker from '../../components/persian_calender/src/index';
import moment_jalaali from 'moment-jalaali'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Selectbox from 'react-native-selectbox'
import AlertModal from '../../components/alertModal'
import Multimedia from '../../components/multimedia'

class Profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            loading: true,
            signed: false,
            showPicker:false,
            selectedStartDate:null,
            fname:this.props.user.fname,
            lname:this.props.user.lname,
            province:0,
            provinceTitle: '',
            town:0,
            townTitle: '',
            mobile:this.props.user.mobile,
            mobile2:"",
            timeIn: {key: 0, label: "انتخاب نشده", value: 0},
            insurances: [],
            provinces: [],
            city: [],
            type: {key: 0, label: "انتخاب نشده", value: 0},
            loading2: false,
            modalVisible: false,
            add: false,
            fillAll: false,
            visibleinput2: false,
            visibleinput3: false,
            fillFname: false,
            fillLname: false,
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
        if(this.state.add){
            Actions.reset('reminders', {openDrawer: this.props.openDrawer});
        }
    }
    onShowinput2 = () => {
        this.setState({ visibleinput2: true });
    }

    onSelectInput2 = (picked) => {
        console.log('picked', picked)
        this.setState({
            province: picked,
            visibleinput2: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.provinces.map((item) => {
                if(picked === item.id) {
                    this.setState({provinceTitle: item.name}, () => {this.getCat(picked);console.log(this.state.provinceTitle)});
                }
            })
        })
    }
    onCancelinput2 = () => {
        this.setState({
            visibleinput2: false
        })
    }
    onShowinput3 = () => {
        this.setState({ visibleinput3: true });
    }

    onSelectInput3 = (picked) => {
        console.log('picked', picked)
        this.setState({
            town: picked,
            visibleinput3: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.city.map((item) => {
                if(picked === item.id) {
                    this.setState({townTitle: item.name}, () => {console.log(this.state.townTitle)});
                }
            })
        })
    }
    onCancelinput3 = () => {
        this.setState({
            visibleinput3: false
        })
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        Axios.get('/request/get_insurance').then(response => {
            this.setState({insurances: response.data.data});
            Axios.post('/request/get-province', {}).then(response => {
                this.setState({ provinces: response.data.data});



                Axios.post('/request/blog/list').then(response => {
                    this.setState({loading: false, spacialPosts: response.data.data.data});
                    console.log('spacial posts', response.data.data.data)

                })
                    .catch((error) => {
                        console.log(error);
                        this.setState({modalVisible: true, loading: false});
                    });




            })
                .catch((error) => {
                    // Alert.alert('','خطا')
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                    console.log(error);
                });
        })
            .catch((error) => {
                // Alert.alert('','خطا')
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});
                console.log(error);
            });
    }
    getCat(id) {
        this.setState({loading2:true});
        Axios.post('/request/get-city', {id: id}).then(response => {
            this.setState({loading2: false, city: response.data.data});
            console.log('kind cars', response.data.data);
        })
            .catch((error) =>{
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading2: false});
                this.setState({modalVisible: true, loading: false});

            });
    }
    addRemind() {
        this.setState({loading: true});
        AsyncStorage.getItem('token').then((info) => {
            const newInfo = JSON.parse(info);
            Axios.post('/request/remember/add', {
                user_id: newInfo.user_id,
                insurance_id: this.state.type.value,
                fname: this.state.fname,
                lname: this.state.lname,
                mobile: this.state.mobile,
                province: this.state.province,
                city: this.state.town,
                reminder: this.state.timeIn.value,
                expired_date: this.state.selectedStartDate
            }).then(response => {
                this.setState({modalVisible: true, loading: false, add: true});
                // Alert.alert('','یادآوری با موفقیت افزوده شد');
                // Actions.reminders();
            })
                .catch((error) =>{
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});

                });
        });
    }
    test() {
        if( this.state.province !== 0 && this.state.town !== 0  && this.state.timeIn.value !== 0 && this.state.type.value !== 0  && this.state.selectedStartDate !== null && this.state.mobile !== '') {
            this.addRemind();
        }
        else if(/^[-]?\d+$/.test(this.state.fname) === true) {
            this.setState({modalVisible: true, fillFname: true});
        }
        else if(/^[-]?\d+$/.test(this.state.lname) === true) {
            this.setState({modalVisible: true, fillLname: true});
        }
        else {
            // Alert.alert('','لطفا تمام موارد را پر نمایید');
            this.setState({modalVisible: true, fillAll: true});
        }
    }
    onChange(){
        this.setState({showPicker: false});
    }
    onDateChange(date) {
        setTimeout(() => {this.setState({showPicker: false})}, 200)
        this.setState({ selectedStartDate: date });
    }
    closeModal2() {
        this.setState({showPicker: false});
    }
    render() {
        const {user} = this.props;
        let Items = [{key: 0, label: "انتخاب نشده", value: 0}];
        this.state.insurances.map((item) => {Items.push({key: item.id, label: item.title, value: item.id})})
        console.log('items', Items)
        // if(this.state.loading){
        //     return (<Loader />)
        // }
        // else
            return (
        <View style={styles.container}>
            {/*<ImageBackground source={hd} style={{flex:1 , width: '100%', height: undefined,  aspectRatio: 1,}}>*/}

            {/*</ImageBackground>*/}
            <View style={{position: 'absolute', top: -3, left: 0, right: 0,  width: '100%',  zIndex: 10, height: 155}}>
                <Image
                    style={{flex:1, height: undefined, width: undefined,}}
                    source={hd}
                    resizeMode="contain"
                />
            </View>
            <View style={{position: 'absolute', top: 0, left: 0, right: 0, paddingRight: 10, paddingLeft: 10,  width: '100%',  zIndex: 20, height: 60, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                <FIcon name="chevron-left" size={28} color="white" />
                <Text style={styles.headerText}>pooliver</Text>
                <Icon name="bars" size={20} color="white" />
            </View>
            <ScrollView style={styles.scroll} nestedScrollEnabled={true}>
                <View style={styles.body}>
                    <View>
                        <Text style={styles.infoLabel}>محمد مهدوی</Text>
                        <Text style={styles.infoLabel}>09399523269</Text>
                    </View>
                    <View style={styles.creadit}>
                        <View style={styles.exit}>
                            <SIcon name="logout" size={16} color="rgb(50, 50, 50)" />
                            <Text style={styles.exitLabel}>خروج</Text>
                        </View>
                        <View style={styles.creaditButton}>
                            <Text style={styles.creaditLabel}>اعتبار 52000 تومان</Text>
                        </View>
                    </View>
                    <View style={[styles.row, { paddingTop: 30, paddingBottom: 15}]}>
                        <Text style={styles.title}>کیف پول من</Text>
                        <SIcon name="wallet" size={24} color="blue" />
                    </View>
                    <View style={styles.wallet}>
                        <Text style={[styles.walletTitle, {paddingRight: 5}]}>تومان</Text>
                        <View style={styles.walletAmount}>
                            <Text style={styles.exitLabel}>52000000</Text>
                        </View>
                        <Text style={[styles.walletTitle, {paddingLeft: 5}]}>موجودی کیف پول شما</Text>
                    </View>
                    <Image source={pr} style={{width: 80, height: 80,  resizeMode: 'stretch', marginTop: 30}} />
                    <Text style={styles.title}>اعتبار رایگان برای هر دعوت</Text>
                    <Text style={styles.contentLabel}>کد معرفی را به دوستانتان معرفی کنید تادر اولین خریدشان از بانی بیمه اعتبار رایگان دریافت کنند. همچنین شما بعد از خریدشان اعتبار رایگان خرید از بانی بیمه دریافت خواهید کرد</Text>

                    <View style={styles.shareContainer}>
                        <TouchableOpacity onPress={() => this.shareCode()} style={styles.shareIcon}>
                            <FIcon name="share-2" size={20} color="white" />
                        </TouchableOpacity>
                        <View style={styles.shareCode}>
                            <Text style={styles.codeValue}>sdfsdfsdf</Text>
                            <Text style={styles.codeLabel}>کد معرفی شما: </Text>
                        </View>
                    </View>
                    <View style={[styles.wallet, {backgroundColor: 'white', width: '94%', borderWidth: 1, borderColor: 'lightgray', marginTop: 30, marginBottom: 15, padding: 5, borderRadius: 3}]}>
                        <View style={styles.sales}>
                            <Text style={[styles.exitLabel, {fontSize: 10, color: 'white'}]}>ارتقا به سمت کارشناس فروش</Text>
                        </View>
                            <Text style={[styles.walletTitle, {paddingLeft: 5, fontSize: 9, color: 'black'}]}>آیا دوست دارید کارشناس فروش ما باشید؟</Text>
                    </View>
                    <View style={styles.tableContainer}>
                        <View style={[styles.row, { marginTop: 20, marginBottom: 20}]}>
                            <Text style={styles.title}>افراد دعوت شده</Text>
                            <FIcon name="users" size={22} color="blue" style={{paddingRight: 15}} />

                            {/*<Image source={us} style={{width: 30, height: 30, resizeMode: 'stretch', tintColor: 'blue', marginRight: 15}} />*/}
                        </View>
                        <View style={styles.inviteContainer}>
                            <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)', 'rgba(62, 64, 219, 1)']} style={styles.container}>
                                <View style={styles.inviteRow}>
                                    <View style={styles.rowItem}>
                                        <Text style={styles.tableTitle}>اعتبار کسب شده</Text>
                                    </View>
                                    <View style={styles.rowItem}>
                                        <Text style={styles.tableTitle}>تاریخ</Text>
                                    </View>
                                    <View style={styles.rowItem}>
                                        <Text style={styles.tableTitle}>نام و نام خانوادگی</Text>
                                    </View>
                                    <View style={styles.numberItem}>
                                        <Text style={styles.tableTitle}>#</Text>
                                    </View>
                                </View>
                            </LinearGradient>
                            <View style={[styles.inviteRow]}>
                                <View style={[styles.rowItem, {borderBottomWidth: 1, borderColor:  'rgb(207, 207, 207)' }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)'}]}>25000 تومان</Text>
                                </View>
                                <View style={[styles.rowItem, {borderBottomWidth: 1, borderLeftWidth: 1, borderColor:  'rgb(207, 207, 207)' }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)'}]}>25/02/1397</Text>
                                </View>
                                <View style={[styles.rowItem, {borderBottomWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor:  'rgb(207, 207, 207)' }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)'}]}>علی ده نمکی</Text>
                                </View>
                                <View style={[styles.numberItem, {borderBottomWidth: 1, borderColor:  'rgb(207, 207, 207)'}]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)'}]}>1</Text>
                                </View>
                            </View>
                            <View style={[styles.inviteRow, {backgroundColor: 'rgb(235, 235, 235)'}]}>
                                <View style={[styles.rowItem, {borderBottomWidth: 1, borderColor:  'rgb(207, 207, 207)' }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)'}]}>25000 تومان</Text>
                                </View>
                                <View style={[styles.rowItem, {borderBottomWidth: 1, borderLeftWidth: 1, borderColor:  'rgb(207, 207, 207)' }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)'}]}>25/02/1397</Text>
                                </View>
                                <View style={[styles.rowItem, {borderBottomWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor:  'rgb(207, 207, 207)' }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)'}]}>علی ده نمکی</Text>
                                </View>
                                <View style={[styles.numberItem, {borderBottomWidth: 1, borderColor:  'rgb(207, 207, 207)'}]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)'}]}>1</Text>
                                </View>
                            </View>
                            <View style={styles.more}>
                                <FIcon name="chevron-down" size={20} color="gray" />
                            </View>
                        </View>
                    </View>

                    <View style={styles.tableContainer}>
                        <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'space-between', padding: 5 , paddingBottom: 20, paddingTop: 20 }}>
                            <View style={{width: '50%'}}>
                                <View style={[styles.sales, {width: '70%', backgroundColor: 'white', borderColor: 'blue', borderWidth: 1, alignItems: 'center', justifyContent: 'center',    paddingTop: 3, paddingBottom: 3}]}>
                                    <Text style={[styles.exitLabel, {fontSize: 10, color: 'blue'}]}>درخواست بن جدید</Text>
                                </View>
                            </View>
                            <View style={[styles.row, {width: '50%'}]}>
                                <Text style={styles.title}>بن های شما</Text>
                                <IIcon name="md-paper" size={22} color="blue" style={{paddingRight: 15}} />
                            </View>
                        </View>

                        <View style={styles.inviteContainer}>
                            <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)', 'rgba(62, 64, 219, 1)']} style={styles.container}>
                                <View style={styles.inviteRow}>
                                    <View style={[styles.rowItem, {width: '20%'}]}>
                                        <Text style={styles.tableTitle2}>وضعیت بن</Text>
                                    </View>
                                    <View style={[styles.rowItem, {width: '20%'}]}>
                                        <Text style={styles.tableTitle2}>میزان تخفیف</Text>
                                    </View>
                                    <View style={[styles.rowItem, {width: '20%'}]}>
                                        <Text style={styles.tableTitle2}>تاریخ</Text>
                                    </View>
                                    <View style={[styles.rowItem, {width: '30%'}]}>
                                        <Text style={styles.tableTitle2}>کد تخفیف</Text>
                                    </View>
                                    <View style={[styles.numberItem, {width: '10%'}]}>
                                        <Text style={styles.tableTitle2}>#</Text>
                                    </View>
                                </View>
                            </LinearGradient>
                            <View style={[styles.inviteRow]}>
                                <View style={[styles.rowItem, {width: '20%', borderBottomWidth: 1, borderColor:  'rgb(207, 207, 207)', borderRightWidth: 1 }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)', fontSize: 9 }]}>25000 تومان</Text>
                                </View>
                                <View style={[styles.rowItem, {width: '20%', borderBottomWidth: 1, borderColor:  'rgb(207, 207, 207)' }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)',  fontSize: 9}]}>25000 تومان</Text>
                                </View>
                                <View style={[styles.rowItem, {width: '20%', borderBottomWidth: 1, borderLeftWidth: 1, borderColor:  'rgb(207, 207, 207)' }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)',  fontSize: 9}]}>25/02/1397</Text>
                                </View>
                                <View style={[styles.rowItem, {width: '30%', borderBottomWidth: 1, borderLeftWidth: 1, borderRightWidth: 1, borderColor:  'rgb(207, 207, 207)' }]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)',  fontSize: 9}]}>علی ده نمکی</Text>
                                </View>
                                <View style={[styles.numberItem, {width: '10%', borderBottomWidth: 1, borderColor:  'rgb(207, 207, 207)'}]}>
                                    <Text style={[styles.tableTitle, {color: 'rgb(50, 50, 50)'}]}>1</Text>
                                </View>
                            </View>
                            <View style={styles.more}>
                                <FIcon name="chevron-down" size={20} color="gray" />
                            </View>
                        </View>
                    </View>
                    <View style={[styles.tableContainer, {height: 320, backgroundColor: 'rgb(246, 250, 253)'}]}>
                        <View style={[styles.row, { marginTop: 20, marginBottom: 20}]}>
                            <Text style={styles.title}>تقویم یادآوری</Text>
                            <FIcon name="calendar" size={18} color="blue" style={{paddingRight: 15}} />

                            {/*<Image source={us} style={{width: 30, height: 30, resizeMode: 'stretch', tintColor: 'blue', marginRight: 15}} />*/}
                        </View>
                        <View style={styles.remindContainer}>
                            <View style={styles.left}>
                                {/*<View style={{*/}
                                {/*// position: 'absolute',*/}
                                {/*// bottom: '65%',*/}
                                {/*// zIndex: 9999,*/}
                                {/*width: '100%',*/}
                                {/*height: 160,*/}
                                {/*backgroundColor: 'rgb(0, 128, 255)',*/}
                                {/*borderRadius: 5*/}
                                {/*}}>*/}
                                <PersianCalendarPicker
                                    initialDate={this.props.input === 1 ? new Date().setFullYear(new Date().getFullYear() - 1) : undefined}
                                    todayBackgroundColor="rgb(250, 189, 58)"
                                    onDateChange={(date) => this.props.onDateChange(date, this.props.input)}
                                    width={150}
                                    height={150}
                                />
                                {/*</View>*/}
                            </View>
                            <View style={styles.right}>
                                <ScrollView style={styles.scroll2} >
                                    <View style={styles.bodyContainer2}>
                                        <View style={{width: '100%', zIndex: 0, paddingBottom: 70}}>
                                            {/*<Text style={styles.text}>نام</Text>*/}
                                            {/*<View style={{position: 'relative', zIndex: 3, width: '100%', backgroundColor: 'yellow'}}>*/}
                                            <View style={{position: 'relative', zIndex: 3, width: '100%', height: 30, borderRadius: 15, backgroundColor: 'white',  borderColor:'rgb(237, 237, 237)', borderWidth: 1, marginBottom: 5}}>
                                                <TextInput
                                                    placeholder="نام"
                                                    placeholderTextColor={'gray'}
                                                    underlineColorAndroid='transparent'
                                                    value={this.state.fname}
                                                    maxLength={12}
                                                    style={{
                                                        height: 32,
                                                        paddingRight: 10,
                                                        width: '100%',
                                                        color: 'gray',
                                                        fontSize: 11,
                                                        textAlign: 'right'
                                                    }}
                                                    // onChangeText={(text) => this.setState({text: text, fill: ++this.state.fill})}
                                                    onChangeText={(text) => {this.setState({fname: text})}}
                                                />
                                            </View>
                                            {/*<Text style={styles.text}>نام خانوادگی</Text>*/}
                                            <View style={{position: 'relative', zIndex: 3, width: '100%', height: 30,  borderRadius: 15, backgroundColor: 'white',   borderColor:'rgb(237, 237, 237)', borderWidth: 1, marginBottom: 5}}>
                                                <TextInput
                                                    placeholder="نام خانوادگی"
                                                    placeholderTextColor={'gray'}
                                                    underlineColorAndroid='transparent'
                                                    value={this.state.lname}
                                                    maxLength={16}
                                                    style={{
                                                        height: 33,
                                                        paddingRight: 15,
                                                        width: '100%',
                                                        color: 'gray',
                                                        fontSize: 11,
                                                        textAlign: 'right',
                                                        // elevation: 4
                                                    }}
                                                    onChangeText={(text) => {this.setState({lname: text})}}
                                                />
                                            </View>

                                            {/*<Text style={styles.text}>تاریخ انقضا</Text>*/}
                                            <TouchableOpacity onPress={() => this.setState({showPicker: true})} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 30,  borderRadius: 15,   borderColor:'rgb(237, 237, 237)', borderWidth: 1, marginBottom: 5}}>
                                                <Text style={{textAlign: 'right', paddingTop: '3%', paddingRight: 10, fontSize: 11}}>{this.state.selectedStartDate !==null ? moment_jalaali(this.state.selectedStartDate).format('jYYYY/jM/jD') : 'تاریخ انقضا'}</Text>
                                                <Icon name="calendar-o" size={10} color="rgb(0, 125, 255)" style={{position: 'absolute', zIndex: 90, top: 7, left: 10}}/>
                                            </TouchableOpacity>

                                            {/*<Text style={styles.text}>استان</Text>*/}
                                            <TouchableOpacity onPress={this.onShowinput2} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 30, borderColor:'rgb(237, 237, 237)', borderWidth: 1, borderRadius: 15, marginBottom: 5}}>
                                                <FIcon name="chevron-down" size={12} color="gray" style={{position: 'absolute', zIndex: 90, top: 7, left: 10}}/>
                                                <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray', fontSize: 10 }}>{this.state.loading ? "در حال بارگذاری..." : this.state.province === 0 ? "انتخاب نشده" : this.state.provinceTitle}</Text>
                                                <ModalFilterPicker
                                                    visible={this.state.visibleinput2}
                                                    onSelect={this.onSelectInput2}
                                                    onCancel={this.onCancelinput2}
                                                    options={ this.state.provinces.map((item) => {return {key: item.id, label: item.name, value: item.id}})}
                                                    placeholderText="جستجو ..."
                                                    cancelButtonText="لغو"
                                                    filterTextInputStyle={{textAlign: 'right'}}
                                                    optionTextStyle={{textAlign: 'right', width: '100%'}}
                                                    titleTextStyle={{textAlign: 'right'}}
                                                />
                                            </TouchableOpacity>

                                            {/*<Text style={styles.text}>شهر</Text>*/}
                                            {/*<TouchableOpacity onPress={this.onShowinput3} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 30, borderColor: this.state.town !== 0 ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1, borderRadius: 5}}>*/}
                                            {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                            {/*<Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray', fontSize: 11 }}>{this.state.loading2 ? "در حال بارگذاری..." : this.state.town === 0 ? "انتخاب نشده" : this.state.townTitle}</Text>*/}
                                            {/*<ModalFilterPicker*/}
                                            {/*visible={this.state.visibleinput3}*/}
                                            {/*onSelect={this.onSelectInput3}*/}
                                            {/*onCancel={this.onCancelinput3}*/}
                                            {/*options={ this.state.city.map((item) => {return {key: item.id, label: item.name, value: item.id}})}*/}
                                            {/*placeholderText="جستجو ..."*/}
                                            {/*cancelButtonText="لغو"*/}
                                            {/*filterTextInputStyle={{textAlign: 'right'}}*/}
                                            {/*optionTextStyle={{textAlign: 'right', width: '100%'}}*/}
                                            {/*titleTextStyle={{textAlign: 'right'}}*/}
                                            {/*/>*/}
                                            {/*</TouchableOpacity>*/}

                                            {/*<Text style={styles.text}>موبایل</Text>*/}
                                            {/*<View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderRadius: 5, backgroundColor: 'white',  borderColor: this.state.mobile !== '' ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>*/}
                                            {/*<TextInput*/}
                                            {/*keyboardType={"numeric"}*/}
                                            {/*placeholder="موبایل"*/}
                                            {/*placeholderTextColor={'gray'}*/}
                                            {/*underlineColorAndroid='transparent'*/}
                                            {/*value={this.state.mobile}*/}
                                            {/*maxLength={11}*/}
                                            {/*style={{*/}
                                            {/*height: 40,*/}
                                            {/*paddingRight: 15,*/}
                                            {/*width: '100%',*/}
                                            {/*color: 'gray',*/}
                                            {/*fontSize: 14,*/}
                                            {/*textAlign: 'right',*/}
                                            {/*// elevation: 4*/}
                                            {/*}}*/}
                                            {/*onChangeText={(text) => {this.setState({mobile: text})}}*/}
                                            {/*/>*/}
                                            {/*</View>*/}

                                            {/*<Text style={styles.text}>موبایل</Text>*/}
                                            {/*<View style={{position: 'relative', zIndex: 3, width: '100%', height: 42, borderRadius: 5, backgroundColor: 'white',   borderColor: this.state.mobile2 !== '' ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>*/}
                                            {/*<TextInput*/}
                                            {/*keyboardType={"numeric"}*/}
                                            {/*placeholder="شماره موبایل دوم(اختیاری)"*/}
                                            {/*placeholderTextColor={'gray'}*/}
                                            {/*underlineColorAndroid='transparent'*/}
                                            {/*value={this.state.mobile2}*/}
                                            {/*maxLength={11}*/}
                                            {/*style={{*/}
                                            {/*height: 40,*/}
                                            {/*paddingRight: 15,*/}
                                            {/*width: '100%',*/}
                                            {/*color: 'gray',*/}
                                            {/*fontSize: 14,*/}
                                            {/*textAlign: 'right',*/}
                                            {/*// elevation: 4*/}
                                            {/*}}*/}
                                            {/*onChangeText={(text) => {this.setState({mobile2: text})}}*/}
                                            {/*/>*/}
                                            {/*</View>*/}
                                            {/*<Text style={styles.text}>نوع بیمه</Text>*/}


                                            {/*<View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 5,  borderColor: this.state.type.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>*/}
                                            {/*/!*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*!/*/}
                                            {/*<Selectbox*/}
                                            {/*style={{height: 42, paddingTop: '3%', paddingRight: 10}}*/}
                                            {/*selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}*/}
                                            {/*optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}*/}
                                            {/*selectedItem={this.state.type}*/}
                                            {/*cancelLabel="لغو"*/}
                                            {/*onChange={(itemValue) =>{*/}
                                            {/*this.setState({*/}
                                            {/*type: itemValue*/}
                                            {/*}, () => {*/}
                                            {/*console.log('item value', itemValue)*/}
                                            {/*})*/}
                                            {/*}}*/}
                                            {/*items={Items} />*/}
                                            {/*</View>*/}
                                            {/*<Text style={styles.text}>زمان یادآوری</Text>*/}

                                            {/*<View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 5,  borderColor: this.state.timeIn.value !== 0 ? 'rgb(50, 197, 117)' : '#ccc', borderWidth: 1}}>*/}
                                            {/*/!*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*!/*/}
                                            {/*<Selectbox*/}
                                            {/*style={{height: 42, paddingTop: '3%', paddingRight: 10}}*/}
                                            {/*selectLabelStyle={{textAlign: 'right', color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}*/}
                                            {/*optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}*/}
                                            {/*selectedItem={this.state.timeIn}*/}
                                            {/*cancelLabel="لغو"*/}
                                            {/*onChange={(itemValue) =>{*/}
                                            {/*this.setState({*/}
                                            {/*timeIn: itemValue*/}
                                            {/*})*/}
                                            {/*}}*/}
                                            {/*items={[*/}
                                            {/*{key: 0, label: "انتخاب نشده", value: 0},*/}
                                            {/*{key: 1, label: "ماهیانه", value: 1},*/}
                                            {/*{key: 2, label: "سه ماهه", value: 3},*/}
                                            {/*{key: 3, label: "شش ماهه", value: 6},*/}
                                            {/*{key: 4, label: "سالیانه", value: 12}*/}
                                            {/*]} />*/}
                                            {/*</View>*/}
                                            {/*<View style={{alignItems: 'center', justifyContent: 'center', paddingTop: 30}}>*/}
                                            {/*<TouchableOpacity onPress={() => this.test()} style={styles.advertise}>*/}
                                            {/*<Text style={styles.buttonTitle}>افزودن</Text>*/}
                                            {/*</TouchableOpacity>*/}
                                            {/*</View>*/}
                                            <LinearGradient
                                                start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={[ 'rgb(246, 93, 23)', 'rgb(255, 150, 35)']} style={{alignItems: 'center', justifyContent: 'center',   width: '55%', borderRadius: 5, alignSelf: 'center', marginTop: 15}}>
                                                <TouchableOpacity onPress={() => this.test()} style={styles.advertise}>
                                                    <Text style={styles.buttonTitle}>افزودن</Text>
                                                </TouchableOpacity>
                                            </LinearGradient>


                                        </View>
                                        <AlertModal
                                            closeModal={(title) => this.closeModal2(title)}
                                            modalVisible={this.state.showPicker}
                                            onDateChange={(date) => this.onDateChange(date)}
                                            onChange={()=> this.onChange()}
                                        />
                                        <AlertView
                                            closeModal={(title) => this.closeModal(title)}
                                            modalVisible={this.state.modalVisible}
                                            onChange={() => this.setState({modalVisible: false})}
                                            title={this.state.add ? 'یادآوری با موفقیت افزوده شد' :(this.state.fillAll ? 'لطفا تمام موارد را پر نمایید' : (this.state.fillFname ? 'نام نمی تواند مقدار عددی داشته باشد' : (this.state.fillLname ? 'نام خانوادگی نمیتواند مقدار عددی داشته باشد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید' )) )}
                                        />
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    </View>

                    <View style={styles.tableContainer}>
                        <View style={[styles.row, { marginTop: 20, marginBottom: 20}]}>
                            <Text style={styles.title}>کسب درآمد چندرسانه ای</Text>
                            <Icon name="bar-chart" size={18} color="blue" style={{paddingRight: 15}} />
                        </View>
                        <View style={{width: '100%'}}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                                <View style={styles.slideContainers}>
                                    {
                                        [
                                            {
                                                "id": 7,
                                                "title": "drtgrdtret",
                                                "description": "ertert",
                                                "content": "<p>erterterter</p>",
                                                "user_id": 12,
                                                "category_id": 1,
                                                "display": "1",
                                                "deleted_at": null,
                                                "created_at": "2019-03-26 15:34:19",
                                                "updated_at": "2019-03-26 15:34:19",
                                                "like": 0,
                                                "special": 0,
                                                "seo_title": "erter",
                                                "seo_description": "erter",
                                                "hit": 0,
                                                "attachments": [
                                                    {
                                                        "id": 815,
                                                        "attachmentable_type": "Modules\\Blog\\Entities\\Blog",
                                                        "attachmentable_id": 7,
                                                        "path": "blogs/3E8W6mVzyQgNxhj6uNkBTc4NbkUc1Kh1rsjHiVHW.jpeg",
                                                        "filename": "image",
                                                        "created_at": "2019-03-26 15:34:19",
                                                        "updated_at": "2019-03-26 15:34:19",
                                                        "mime": "jpeg",
                                                        "uid": "blogs5c9a0733442dc"
                                                    }
                                                ],
                                                "liked": false,
                                                "likes": 0,
                                                "commentcount": 0,
                                                "category": {
                                                    "id": 1,
                                                    "title": "بیمه",
                                                    "description": "blog1blog1",
                                                    "deleted_at": null,
                                                    "created_at": "2018-08-23 10:15:15",
                                                    "updated_at": "2018-08-23 10:15:15",
                                                    "attachments": []
                                                }
                                            },
                                            {
                                                "id": 6,
                                                "title": "واکنش بانک مرکزی به تصمیم شورای عالی آمار: تولید و انتشار آمار را متوقف نمی کنیم",
                                                "description": "مدیر اداره آمار اقتصادی بانک مرکزی گفت: مبنای فعالیت‌های آماری بانک مرکزی بند (پ) ماده ۱۰ احکام دائمی برنامه‌های توسعه کشور است و این بانک به فعالیت‌ خود در زمینه تولید و انتشار آمار ادامه می",
                                                "content": "<p>به گزارش&nbsp;<a href=\"https://www.tasnimnews.com/\" target=\"_blank\">خبرگزاری تسنیم</a>&nbsp;به نقل از روابط عمومی بانک مرکزی، محمود چلویان با بیان اینکه این بانک با سابقه هشتاد ساله تولید آمارهای اقتصادی به ویژه نرخ تورم با احتساب سابقه آماری در بانک ملی به عنوان یکی از تولیدکننده&zwnj;های مهم آمار در کشور همواره ایفای نقش کرده است افزود: در این مدت عموم مردم، دستگاه های اجرایی، محققان، دانشگاه ها و مراجع قضایی از آمارهای بانک مرکزی استفاده کرده&zwnj;اند.<br />\r\n<br />\r\nچلویان با تاکید بر اینکه مرکز آمار ایران هم در سال&zwnj;های اخیر در کنار بانک مرکزی مبادرت به انتشار نرخ تورم کرده است، گفت: طبیعتاً به دلایل مختلف نظیر سال پایه، طرح&zwnj;های مربوط به احصای هزینه&zwnj;های خانوار، ترکیب سبد اقلام و ضرایب آن ها و نظام جمع آوری داده&zwnj;ها بین نرخ تورم بانک مرکزی و مرکز آمار تفاوت&zwnj;هایی وجود داشته است. در همین راستا، اخیراً بانک مرکزی تصمیم گرفت در جهت انجام بررسی&zwnj;های کارشناسی مشترک به منظور کاهش اختلافات، موقتا نرخ تورم را منتشر نکند.<br />\r\n<br />\r\nوی افزود: متعاقبا تلاش شد که جلساتی با مرکز آمار برگزار شود لیکن این تلاش&zwnj;ها بی&zwnj;پاسخ ماند و مرکز آمار تمایلی در این خصوص نشان نداد و به جای برگزاری جلسه مشترک، موضوع را به جلسه کمیسیون تخصصی شورای عالی آمار ارجاع داد. البته بخش مهمی از اعضای کمیسیون با طرح این موضوع در جلسه مذکور موافق نبودند.</p>\r\n\r\n<blockquote><strong>بیشتر بخوانید</strong>\r\n\r\n<ul>\r\n\t<li>\r\n\t<p><a href=\"https://tasnimnews.com/1917592\"><strong>شورای عالی آمار مانع انتشار آمار بانک مرکزی شد/ مرکز آمار در پی انحصارطلبی است؟</strong></a></p>\r\n\t</li>\r\n</ul>\r\n</blockquote>\r\n\r\n<p><br />\r\n<br />\r\n<br />\r\nاین مقام مسئول بانک مرکزی با اشاره به مفاد قانونی تولید و انتشار آمار توسط بانک مرکزی گفت: ذکر این نکته ضروری است که بعد از تجربه ناموفق ماده 54 قانون برنامه پنجم توسعه،&nbsp; قانونگذار در جهت اصلاح قانون و تبیین نقش بانک مرکزی در تولید نرخ تورم و رشد اقتصادی ماده 10 قانون احکام دائمی برنامه های توسعه کشور را مصوب و در بند &quot;پ&quot;&nbsp; صراحتا بانک مرکزی را مکلف به تولید&nbsp; و انتشار آمارهای مذکور کرد و اعلام داشت که این آمار، آمار تخصصی رسمی کشور خواهد بود.<br />\r\n<br />\r\nمدیر اداره آمار اقتصادی بانک مرکزی با تشریح سه نکته در رد مصوبه کمیسیون تخصصی شورا مبنی بر عدم تولید و انتشار آمار از سوی بانک مرکزی گفت:&nbsp;اولاً کمیسیون تخصصی شورای عالی آمار نمی&zwnj;تواند خلاف قانون مذکور مصوبه&zwnj;ای داشته باشد. ثانیاً در مذاکرات کمیسیون بودجه و محاسبات مجلس در زمان تصویب ماده 10 احکام دائمی دلیل اصلی وجود بند (پ) اساسا موضوع تولید و انتشار نرخ تورم و رشد اقتصادی بانک مرکزی بوده است و البته شرح مذاکرات در این زمینه نیز موجود است و ثالثاً تفسیر قوانین در حیطه وظایف مجلس محترم شورای اسلامی است نه کمیسیون تخصصی شورای عالی آمار.<br />\r\n<br />\r\nوی در پایان با تاکید بر تولید و انتشار آمار بر مبنای مفاد قانونی توسط بانک مرکزی گفت: لذا&nbsp;مبنای فعالیت&zwnj;های آماری بانک مرکزی بند (پ) ماده 10 احکام دائمی برنامه&zwnj;های توسعه کشور خواهد بود و این بانک با قوت به فعالیت&zwnj;های قانونی خود در زمینه تولید و انتشار آمار و پاسخگویی به نیازهای جامعه ادامه خواهد داد. البته در راستای کاهش تفاوت&zwnj;های بین مراجع آماری و به منظور پرهیز از سردرگمی مخاطبان کماکان این آمادگی را دارد تا نسبت به بررسی&zwnj;های فنی و کارشناسی مشترک اقدام کند.</p>",
                                                "user_id": 12,
                                                "category_id": 1,
                                                "display": "1",
                                                "deleted_at": null,
                                                "created_at": "2019-01-08 15:15:08",
                                                "updated_at": "2019-01-08 15:15:08",
                                                "like": 0,
                                                "special": 0,
                                                "seo_title": "واکنش بانک مرکزی به تصمیم شورای عالی آمار: تولید و انتشار آمار را متوقف نمی کنیم",
                                                "seo_description": "واکنش بانک مرکزی به تصمیم شورای عالی آمار: تولید و انتشار آمار را متوقف نمی کنیم",
                                                "hit": 0,
                                                "attachments": [
                                                    {
                                                        "id": 777,
                                                        "attachmentable_type": "Modules\\Blog\\Entities\\Blog",
                                                        "attachmentable_id": 6,
                                                        "path": "blogs/VrhAhG2hGJxJdRTLE5bd4RcmMza9XYMDFjLOvWXb.jpeg",
                                                        "filename": "image",
                                                        "created_at": "2019-01-08 15:15:08",
                                                        "updated_at": "2019-01-08 15:15:08",
                                                        "mime": "jpeg",
                                                        "uid": "blogs5c346a1c0dd79"
                                                    }
                                                ],
                                                "liked": false,
                                                "likes": 0,
                                                "commentcount": 0,
                                                "category": {
                                                    "id": 1,
                                                    "title": "بیمه",
                                                    "description": "blog1blog1",
                                                    "deleted_at": null,
                                                    "created_at": "2018-08-23 10:15:15",
                                                    "updated_at": "2018-08-23 10:15:15",
                                                    "attachments": []
                                                }
                                            },
                                            {
                                                "id": 5,
                                                "title": "افزایش تعداد ارزهای دولتی/ ۸ کشور جدید به فهرست بانک مرکزی اضافه شد",
                                                "description": "بانک مرکزی تعداد ارزهای دولتی را از ۳۹ به ۴۷ ارز افزایش داد که با این حساب ارز ۸ کشور جدید به فهرست ارزهای دولتی اضافه شد.",
                                                "content": "<p>به گزارش خبرنگار اقتصادی&nbsp;<a href=\"https://www.tasnimnews.com/\" target=\"_blank\">خبرگزاری تسنیم</a>، متقاضیان دریافت ارزهای دولتی در صورتی که واجد شرایط دریافت این ارزها باشند می&zwnj;توانند هر روز به مرکز مبادلات ارزی کشور مراجعه و نسبت به ارائه درخواست و دریافت ارز مورد نیاز خود اقدام کنند.</p>\r\n\r\n<p>بانک مرکزی روزانه نسبت به اعلام&nbsp;<a href=\"https://www.tasnimnews.com/fa/currency\" target=\"_blank\">نرخ ارز</a>های دولتی اقدام می&zwnj;کند؛ پیش از این هر روز بانک مرکزی نرخ رسمی 39 ارز موجود در مرکز مبادلات ارزی را اعلام می&zwnj;کرد اما از یکی دو روز قبل، نرخ 8 ارز جدید به فهرست ارزهای دولتی اضافه شده است.</p>\r\n\r\n<p>با این حساب، زین پس 47 ارز دولتی به متقاضیان فروخته می&zwnj;شود؛ به&zwnj;تازگی 8 ارز از جمله دلار نیوزیلند، تاکای بنگلادش، کیات میانمار، دینار اردن، لاری گرجستان، روپیه اندونزی، پزوی فیلیپین و منات ترکمنستان به لیست ارزهای دولتی اضافه شده است.</p>\r\n\r\n<p>پیش از این در مرکز مبادلات ارزی کشور، دلار آمریکا، پوند انگلیس، یورو، فرانک سوئیس، کرون سوئد، کرون نروژ، کرون دانمارک، روپیه هند،&nbsp;<a href=\"https://www.tasnimnews.com/fa/currency\" target=\"_blank\">درهم امارات</a>&nbsp;متحده عربی، دینار کویت، روپیه پاکستان، ین ژاپن، دلار هنگ&zwnj;کنگ، ریال عمان، دلار کانادا، راند آفریقای جنوبی، لیر ترکیه، روبل روسیه، ریال قطر، دینار عراق، لیر سوریه، دلار استرالیا،&zwnj; ریال سعودی، دلار سنگاپور،&zwnj; روپیه سریلانکا، روپیه نپال، درام ارمنستان، دینار لیبی، یوان چین،&zwnj; بات تایلند،&zwnj; رینگیت مالزی، وون کره جنوبی، تنگه قزاقستان،&zwnj; افغانی افغانستان،&zwnj; روبل جدید بلاروس، منات آذربایجان،&zwnj; سومونی تاجیکستان، بولیوار جدید ونزوئلا برای فروش به واجدان شرایط دریافت ارزهای دولتی عرضه می&zwnj;شد.</p>",
                                                "user_id": 12,
                                                "category_id": 2,
                                                "display": "1",
                                                "deleted_at": null,
                                                "created_at": "2019-01-08 15:13:29",
                                                "updated_at": "2019-01-08 15:13:29",
                                                "like": 0,
                                                "special": 1,
                                                "seo_title": "افزایش تعداد ارزهای دولتی/ ۸ کشور جدید به فهرست بانک مرکزی اضافه شد",
                                                "seo_description": "افزایش تعداد ارزهای دولتی/ ۸ کشور جدید به فهرست بانک مرکزی اضافه شد",
                                                "hit": 0,
                                                "attachments": [
                                                    {
                                                        "id": 776,
                                                        "attachmentable_type": "Modules\\Blog\\Entities\\Blog",
                                                        "attachmentable_id": 5,
                                                        "path": "blogs/ek9wJXn4lzsYI9r9O1YD2DfIF8F0MSy4MDFqChIL.jpeg",
                                                        "filename": "image",
                                                        "created_at": "2019-01-08 15:13:29",
                                                        "updated_at": "2019-01-08 15:13:29",
                                                        "mime": "jpeg",
                                                        "uid": "blogs5c3469b95456c"
                                                    }
                                                ],
                                                "liked": false,
                                                "likes": 0,
                                                "commentcount": 0,
                                                "category": {
                                                    "id": 2,
                                                    "title": "بیمه بدنه",
                                                    "description": "tagzie",
                                                    "deleted_at": null,
                                                    "created_at": "2018-08-23 10:16:19",
                                                    "updated_at": "2018-08-23 10:16:19",
                                                    "attachments": []
                                                }
                                            },
                                            {
                                                "id": 4,
                                                "title": "قیمت خرید دلار در بانک‌ها امروز ۹۷/۱۰/۱۸| افزایش قیمت خرید تمام ارزها",
                                                "description": "به گزارش خبرنگار اقتصادی خبرگزاری تسنیم ، بعد از شتاب ریزش قیمت ها در بازار ارز و افزایش مراجعه مردم",
                                                "content": "<p>به گزارش خبرنگار اقتصادی&nbsp;<a href=\"https://www.tasnimnews.com/\" target=\"_blank\">خبرگزاری تسنیم</a>&nbsp;، بعد از شتاب ریزش قیمت ها در بازار ارز و افزایش مراجعه مردم به مراکز خرید و فروش ارز برای فروش ارزهای خانگی، بانک مرکزی از بانک ها خواست نسبت به خرید ارزهای مردمی فعال شوند.</p>\r\n\r\n<p>برهمین اساس شعب ارزی بانک ها هر روز بعد از دریافت نرخ از بانک مرکزی، نسبت به خرید ارز از مردم اقدام می کنند. دستور بانک مرکزی به بانک ها بعد از آن صادر شد که صرافی ها از خرید ارزهای مردمی خودداری و میدان را به دلال ها واگذار کرده بودند.</p>\r\n\r\n<p>امروز بانک های عامل&nbsp;<a href=\"https://www.tasnimnews.com/fa/currency\" target=\"_blank\">قیمت دلار</a>&nbsp;را 10 هزار و 801 تومان و 9 ریال، قیمت یورو را 12 هزار و 358 تومان و قیمت پوند انگلیس را 13 هزار 784 تومان و 4 ریال برای خرید از مردم اعلام کرده اند.</p>\r\n\r\n<p>قیمت خرید دلار نسبت به روز قبل، 198 تومان،&nbsp;<a href=\"https://www.tasnimnews.com/fa/currency\" target=\"_blank\">قیمت یورو</a>&nbsp;248 تومان و<a href=\"https://www.tasnimnews.com/fa/currency\" target=\"_blank\">قیمت پوند</a>&nbsp;476 تومان افزایش یافته است.</p>\r\n\r\n<p>نرخ خرید ارز در بانک&zwnj;ها، از زمان اعلام تا مدتی عموما رو به افزایش بود؛ به طوری&zwnj;که نرخ خرید دلار در شعب ارزی بانک&zwnj;ها در ابتدا از 9500 تومان آغاز شد و به بیش از 12 هزار و 500 تومان نیز رسید، اما از حدود یک ماه قبل بانک&zwnj;ها نرخ خرید دلار را کاهش دادند.</p>",
                                                "user_id": 12,
                                                "category_id": 1,
                                                "display": "1",
                                                "deleted_at": null,
                                                "created_at": "2019-01-08 15:11:43",
                                                "updated_at": "2019-01-08 15:11:43",
                                                "like": 0,
                                                "special": 1,
                                                "seo_title": "قیمت خرید دلار در بانک‌ها امروز ۹۷/۱۰/۱۸| افزایش قیمت خرید تمام ارزها",
                                                "seo_description": "قیمت خرید دلار در بانک‌ها امروز ۹۷/۱۰/۱۸| افزایش قیمت خرید تمام ارزها",
                                                "hit": 0,
                                                "attachments": [
                                                    {
                                                        "id": 774,
                                                        "attachmentable_type": "Modules\\Blog\\Entities\\Blog",
                                                        "attachmentable_id": 4,
                                                        "path": "blogs/EedruWbGElgd44FhnSVNucbo6332Hra5fT30XWdS.jpeg",
                                                        "filename": "image",
                                                        "created_at": "2019-01-08 15:11:43",
                                                        "updated_at": "2019-01-08 15:11:43",
                                                        "mime": "jpeg",
                                                        "uid": "blogs5c34694ff1605"
                                                    }
                                                ],
                                                "liked": false,
                                                "likes": 0,
                                                "commentcount": 0,
                                                "category": {
                                                    "id": 1,
                                                    "title": "بیمه",
                                                    "description": "blog1blog1",
                                                    "deleted_at": null,
                                                    "created_at": "2018-08-23 10:15:15",
                                                    "updated_at": "2018-08-23 10:15:15",
                                                    "attachments": []
                                                }
                                            },
                                            {
                                                "id": 3,
                                                "title": "رئیس بانک جهانی کناره گیری کرد",
                                                "description": "رئیس بانک جهانی ۳ سال پیش از به پایان رسیدن دوره ریاستش بر بانک جهانی از سمت خود کناره‌گیری کرد.",
                                                "content": "<p>به گزارش&nbsp;<a href=\"https://www.tasnimnews.com/\" target=\"_blank\">خبرگزاری تسنیم</a>&nbsp;به نقل از سی ان ان، جیم یانگ کیم رئیس بانک جهانی سه سال پیش از به سر رسیدن دوره ریاستش بر این بانک از سمت خود استعفا کرد.</p>\r\n\r\n<p>استعفای کیم فرصتی را برای دونالد ترامپ رئیس جمهور آمریکا فراهم می کند تا گزینه مورد نظر خود را برای ریاست بر این نهاد بین المللی انتخاب کند. در عین حال پیش بینی می شود شاهد کشمکشی بین آمریکا و سایر کشورها برای انتخاب رئیس جدید این بانک باشیم.</p>\r\n\r\n<p>ترامپ بارها از عملکرد نهادهای بین المللی انتقاد کرده و کمک های مالی آمریکا به این نهادها را زیر سوال برده است. رئیس جمهور آمریکا به ویژه برای جلب نظر کشورهای درحال توسعه برای انتخاب گزینه مورد نظر خود در بانک جهانی کار سختی خواهد داشت. این کشورها طی سال های اخیر از سلطه فزاینده آمریکا بر بانک جهانی و سایر نهادهای بین المللی انتقاد کرده اند.</p>\r\n\r\n<p>کیم در سال 2012 ریاست بانک جهانی را در دست گرفت و قرار بود ماموریت وی در این بانک تا سال 2021 ادامه داشته باشد. کیم که توسط باراک اوباما رئیس جمهور سابق آمریکا به این سمت انتخاب شده بود دلیلی برای استعفای خود ذکر نکرده است.</p>",
                                                "user_id": 12,
                                                "category_id": 2,
                                                "display": "1",
                                                "deleted_at": null,
                                                "created_at": "2019-01-08 15:10:36",
                                                "updated_at": "2019-01-08 15:10:36",
                                                "like": 0,
                                                "special": 1,
                                                "seo_title": "رئیس بانک جهانی کناره گیری کرد",
                                                "seo_description": "رئیس بانک جهانی کناره گیری کرد",
                                                "hit": 0,
                                                "attachments": [
                                                    {
                                                        "id": 775,
                                                        "attachmentable_type": "Modules\\Blog\\Entities\\Blog",
                                                        "attachmentable_id": 3,
                                                        "path": "blogs/SfS3E8W6mVzyQgNxhj6uNkBTc4NbkUc1Kh1rs4tN.jpeg",
                                                        "filename": "image",
                                                        "created_at": "2019-01-08 15:12:07",
                                                        "updated_at": "2019-01-08 15:12:07",
                                                        "mime": "jpeg",
                                                        "uid": "blogs5c346967c6166"
                                                    }
                                                ],
                                                "liked": false,
                                                "likes": 0,
                                                "commentcount": 0,
                                                "category": {
                                                    "id": 2,
                                                    "title": "بیمه بدنه",
                                                    "description": "tagzie",
                                                    "deleted_at": null,
                                                    "created_at": "2018-08-23 10:16:19",
                                                    "updated_at": "2018-08-23 10:16:19",
                                                    "attachments": []
                                                }
                                            },
                                            {
                                                "id": 2,
                                                "title": "قیمت طلا، قیمت سکه و قیمت ارز امروز ۹۷/۱۰/۱۸",
                                                "description": "قیمت سکه تمام‌بهار آزادی در بازار تهران امروز سه شنبه با نوسان جزئی نسبت به دیروز، ۳ میلیون و ۷۴۸ هزار تومان شد.",
                                                "content": "<p>به گزارش خبرنگار اقتصادی&nbsp;<a href=\"https://www.tasnimnews.com/\" target=\"_blank\">خبرگزاری تسنیم</a>&nbsp;،امروز در بازار آزاد قیمت انواع سکه و طلا نسبت به روز گذشته نوسان جزئی داشته است؛ برهمین اساس در بازار تهران سکه تمام بهار آزادی طرح جدید در قیمت 3 میلیون و 748 هزار تومان و سکه تمام بهار آزادی طرح قدیم با قیمت 3 میلیون و 640 هزار تومان معامله می&zwnj;شود.</p>\r\n\r\n<p>در بازار آزاد هر قطعه&nbsp;<a href=\"https://www.tasnimnews.com/fa/currency\" target=\"_blank\">نیم سکه بهار آزادی</a>&nbsp;یک میلیون و 950 هزار تومان، ربع سکه یک&zwnj;میلیون و 200 هزار تومان و هر قطعه سکه گرمی 660 هزار تومان فروخته می&zwnj;شود.</p>\r\n\r\n<p>هر گرم طلای 18عیار هم 340 هزار و 950&nbsp; تومان ارزش&zwnj;گذاری شده است، ضمن آنکه هر&nbsp;<a href=\"https://www.tasnimnews.com/fa/currency\" target=\"_blank\">اونس طلا</a>&nbsp;در بازارهای جهانی با قیمت 1283.3 دلار معامله می&zwnj;شود.</p>\r\n\r\n<p>** قیمت های درج شده برای انواع سکه و طلا از اتحادیه طلا و جواهر گرفته شده است.</p>\r\n\r\n<p>گفتنی است، صرافی&zwnj;های بانک ها&nbsp;<a href=\"https://www.tasnimnews.com/fa/currency\" target=\"_blank\">قیمت دلار</a>&nbsp;را برای روز جاری با 200 تومان افزایش نسبت به نرخ دیروز، 10&nbsp; هزار و 900 تومان اعلام کرده&zwnj;اند. این صرافی&zwnj;ها دلار را از مردم با قیمت 10 هزار و 800 تومان خریداری می&zwnj;کنند.</p>\r\n\r\n<p>قیمت فروش یورو نیز در صرافی&zwnj;های بانک ها 12 هزار و 750 تومان اعلام شده است؛ قیمت خرید یورو&nbsp; نیز در این صرافی&zwnj;ها 12 هزار و 650 تومان است.</p>\r\n\r\n<p>یورو در مرکز مبادلات ارزی 4840 تومان،&zwnj;&nbsp;<a href=\"https://www.tasnimnews.com/fa/currency\" target=\"_blank\">درهم امارات</a>&nbsp;1143 تومان و پوند انگلیس نیز 5359 تومان قیمت خورده و به واجدان شرایط دریافت ارزهای دولتی فروخته می&zwnj;شود.</p>",
                                                "user_id": 12,
                                                "category_id": 1,
                                                "display": "1",
                                                "deleted_at": null,
                                                "created_at": "2019-01-08 15:09:03",
                                                "updated_at": "2019-01-08 15:09:03",
                                                "like": 0,
                                                "special": 1,
                                                "seo_title": "قیمت طلا، قیمت سکه و قیمت ارز امروز ۹۷/۱۰/۱۸",
                                                "seo_description": "قیمت طلا، قیمت سکه و قیمت ارز امروز ۹۷/۱۰/۱۸",
                                                "hit": 0,
                                                "attachments": [
                                                    {
                                                        "id": 773,
                                                        "attachmentable_type": "Modules\\Blog\\Entities\\Blog",
                                                        "attachmentable_id": 2,
                                                        "path": "blogs/tzBxQ8xLEadjtUZNKI1FOgq4LqeETIEDDXsbf80T.png",
                                                        "filename": "image",
                                                        "created_at": "2019-01-08 15:09:03",
                                                        "updated_at": "2019-01-08 15:12:39",
                                                        "mime": "png",
                                                        "uid": "blogs5c346987d0a64"
                                                    }
                                                ],
                                                "liked": false,
                                                "likes": 0,
                                                "commentcount": 0,
                                                "category": {
                                                    "id": 1,
                                                    "title": "بیمه",
                                                    "description": "blog1blog1",
                                                    "deleted_at": null,
                                                    "created_at": "2018-08-23 10:15:15",
                                                    "updated_at": "2018-08-23 10:15:15",
                                                    "attachments": []
                                                }
                                            },
                                            {
                                                "id": 1,
                                                "title": "کامبیز پیکار جو به عنوان گزینه مدیرعاملی معرفی شد / انتخاب بیانیان در سمت رئیس هیات مدیره",
                                                "description": "با تصمیم هیئت مدیره جدید شرکت بیمه آرمان ، کامبیز پیکارجو بعنوان مدیرعامل این شرکت به بیمه مرکزی معرفی شد .",
                                                "content": "<p>با تصمیم هیئت مدیره جدید شرکت بیمه آرمان ، کامبیز پیکارجو بعنوان مدیرعامل این شرکت به بیمه مرکزی معرفی شد .</p>\r\n\r\n<p>به گزارش ریسک نیوز،با تصمیم هیئت مدیره جدید شرکت بیمه آرمان ، کامبیز پیکارجو بعنوان مدیرعامل این شرکت به بیمه مرکزی معرفی شد همچنین در پی تغییرات در هیئت مدیره بیمه آرمان ، علیرضا بیانیان به عنوان رئیس هیئت مدیره بیمه آرمان انتخاب شد .<br />\r\n<br />\r\n&nbsp;پیکارجو که پیش از این بعنوان قائم مقام مدیرعامل به بیمه آرمان پیوسته بود ، در مجمع اخیر این شرکت به عضویت هیئت مدیره انتخاب و امروز رسما بعنوان مدیرعامل معرفی شد.<br />\r\n<br />\r\nاز سوابق اجرایی وی می توان به عضویت هیئت مدیره بیمه سینا ، قائم مقام مدیرعامل بیمه سینا ، مدیر تحقیق و توسعه بیمه ملت اشاره کرد.<br />\r\n<br />\r\nدر سالهای آغازین فعالیت پژوهشکده بیمه وی بعنوان مشاور با این نهاد همکاری داشت.<br />\r\n<br />\r\nپیکارجو از اساتید دانشکده مدیریت و اقتصاد دانشگاه آزاد اسلامی واحد علوم تحقیقات می باشد.</p>\r\n\r\n<p>به گزارش ریسک نیوز ، کتاب شناخت پدیده بیمه بانک در دنیا و ابعاد مختلف آن در کارنامه عملکردی کامبیز پیکارجو ثبت شده است .</p>\r\n\r\n<p>لازم بذکر است این کتاب در سال ۹۳ با قلم دکتر علی حسن زاده و کامبیز پیکارجو توسط انتشارات پژوهشکده بیمه به چاپ رسید.</p>\r\n\r\n<p>این کتاب شامل 5 فصل با عناوین زیرمی باشد:<br />\r\nفصل اول: آشنایی با بیمه بانک در جهان، فصل دوم: بررسی ساختار بازار مورد فعالیت های نهادهای بیمه بانک در جهان،&zwnj;فصل سوم بررسی ساختارهای قانون و قوانین در بازارهای بیمه بانک در کشورهای موفق جهان، فصل چهارم: بررسی تجارب، دستاوردها و موفقیت های نهادهای بیمه بانک و فصل پنجم نتیجه گیری می پردازد.</p>",
                                                "user_id": 12,
                                                "category_id": 1,
                                                "display": "1",
                                                "deleted_at": null,
                                                "created_at": "2019-01-08 15:08:04",
                                                "updated_at": "2019-01-08 15:08:04",
                                                "like": 0,
                                                "special": 0,
                                                "seo_title": "با تصمیم هیئت مدیره جدید شرکت بیمه آرمان ، کامبیز پیکارجو بعنوان مدیرعامل این شرکت به بیمه مرکزی معرفی شد .",
                                                "seo_description": "با تصمیم هیئت مدیره جدید شرکت بیمه آرمان ، کامبیز پیکارجو بعنوان مدیرعامل این شرکت به بیمه مرکزی معرفی شد .",
                                                "hit": 0,
                                                "attachments": [
                                                    {
                                                        "id": 772,
                                                        "attachmentable_type": "Modules\\Blog\\Entities\\Blog",
                                                        "attachmentable_id": 1,
                                                        "path": "blogs/03E8W6mVzyQgNxhj6uNkBTc4NbkUc1Kh1rs7s23j.jpeg",
                                                        "filename": "image",
                                                        "created_at": "2019-01-08 15:08:05",
                                                        "updated_at": "2019-01-08 15:08:05",
                                                        "mime": "jpeg",
                                                        "uid": "blogs5c34687506d66"
                                                    }
                                                ],
                                                "liked": false,
                                                "likes": 0,
                                                "commentcount": 0,
                                                "category": {
                                                    "id": 1,
                                                    "title": "بیمه",
                                                    "description": "blog1blog1",
                                                    "deleted_at": null,
                                                    "created_at": "2018-08-23 10:15:15",
                                                    "updated_at": "2018-08-23 10:15:15",
                                                    "attachments": []
                                                }
                                            }
                                        ].map((item) => {return <Multimedia item={item} key={item.id}  />})
                                    }

                                </View>
                            </ScrollView>
                        </View>
                    </View>
                    <AlertView
                        closeModal={(title) => this.closeModal(title)}
                        modalVisible={this.state.modalVisible}
                        title={this.state.signed ?  'لطفا برای مشاهده این بخش ابتدا ثبت نام کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                    />
                </View>
            </ScrollView>
            <FooterMenu active="profile" openDrawer={this.props.openDrawer} />
            <LinearGradient
                start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']}>
                <View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />
            </LinearGradient>
        </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(Profile);
