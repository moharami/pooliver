import { StyleSheet, Dimensions } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'flex-end',
        position: 'relative',
        zIndex: 0,
        backgroundColor: 'rgb(246, 250, 253)'

    },
    image: {
        width: '100%',
        height: '25%',
        // height: '36%'
        // resizeMode: 'contain',
        // backgroundColor: 'rgb(61, 99, 223)',
    },
    headerText: {
        fontSize: 17,
        color: 'rgba(255, 255, 255, .5)',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    TrapezoidStyle: {
        flex: 1,
        position: 'absolute',
        bottom: '75%',
        right: 0,
        left: 0,
        zIndex: 100,
        borderTopWidth: 50,
        borderLeftWidth: 0,
        borderBottomWidth: 0,
        borderTopColor: 'transparent',
        borderRightColor: 'rgb(233, 233, 233)',
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
    },
    imageRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        paddingTop: 20
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        width: 200,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: '20%'
    },
    name: {
        fontSize: 15,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10,
        paddingLeft: 20
    },
    infoLabel: {
        fontSize: 13,
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        // paddingTop: 10,
        // paddingLeft: 20
    },
    scroll: {
        position: 'absolute',
        flex: 1,
        top: 60,
        right: 0,
        left: 0,
        bottom: 0,
        zIndex: 5,
        marginBottom: 60
    },
    body : {
        // paddingTop: '38%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    creadit: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',

    },
    creaditButton: {
        backgroundColor: 'rgb(51, 197, 117)',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderWidth: 1,
        borderColor: 'transparent',
        paddingRight: 15,
        paddingLeft: 15,
        padding: 4,
        marginLeft: '15%'
    },
    creaditLabel: {
        fontSize: 12,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
    },
    exit: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    exitLabel: {
        fontSize: 12,
        color: 'rgb(90, 90, 90)',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingLeft: 5
    },
    title: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 10
    },
    contentLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(70, 70, 70)',
        fontSize: 11,
        textAlign: 'center',
        paddingLeft: 5,
        // lineHeight: 20,
        paddingBottom: 15,
        paddingTop: 15,
    },
    wallet: {
        flexDirection: 'row',
        width: '85%',
        alignSelf: 'center',
        backgroundColor: 'rgb(242, 108, 79)',
        borderRadius: 8,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
    walletAmount: {
        backgroundColor: 'white',
        borderRadius: 3,
        paddingRight: 30,
        paddingLeft: 30
    },
    sales: {
        backgroundColor: 'rgb(154, 193, 34)',
        borderRadius: 3,
        paddingRight: 5,
        paddingLeft: 5,
        paddingTop: 6,
        paddingBottom: 6,
    },
    walletTitle: {
        fontSize: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    shareContainer: {
        width: '85%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // backgroundColor: 'rgba(235, 235, 235, 1)',
        height: 40,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'rgb(227, 227, 227)'
    },
    shareIcon: {
        backgroundColor: 'rgb(183, 188, 202)',
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        width: 40

    },
    shareCode: {
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: '',
        paddingRight: 10
    },
    codeLabel: {
        fontSize: 13,
        color: 'rgb(150, 150, 150)',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    codeValue: {
        fontSize: 13,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 15
    },
    tableContainer: {
        borderRadius: 3,
        borderWidth: 1,
        borderColor: 'rgb(227, 227, 227)',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20,
        backgroundColor: 'white'
    },
    inviteContainer: {
        width: '100%',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: 'rgb(227, 227, 227)',
    },
    inviteRow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    numberItem: {
        width: '10%',
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 10,
        height: 30
    },
    rowItem: {
        width: '30%',
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 10,
        height: 30
    },
    more: {
        width: Dimensions.get('window').width*.935,
        height: 16,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomRightRadius: 3,
        borderBottomLeftRadius: 3,
        backgroundColor: 'rgb(214, 223, 248)'
    },
    tableTitle: {
        fontSize: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    tableTitle2: {
        fontSize: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center'
    },
    remindContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    left: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 80
    },
    right: {
        width: '50%',
    },
    bodyContainer2: {
        // paddingBottom: 170,
        // borderWidth: 1,
        // borderColor: 'rgb(237, 237, 237)',
        paddingRight: 5,
        paddingLeft: 5,
        // paddingTop: 20,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    scroll2: {
        // paddingTop: 60,
        // paddingBottom: 120,
        // paddingRight: 15,
        // paddingLeft: 15,
    },
    text: {
        fontSize: 7,
        // color: 'rgba(0, 125, 255, .9)',
        color: 'rgba(122, 130, 153, 1)',
        paddingTop: 8,
        paddingBottom: 4,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    picker: {
        height: 40,
        backgroundColor: 'white',
        width: '100%',
        color: 'gray',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
        marginBottom: 20,
        // overflow: 'hidden'
    },
    telpicker: {
        height: 40,
        backgroundColor: 'white',
        width: '25%',
        color: 'gray',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
    },
    advertise: {
        width: '65%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
        // backgroundColor: 'rgba(255, 193, 39, 1)',
        paddingRight: 10,
        paddingLeft: 10,
        padding: 2
    },
    buttonTitle: {
        fontSize: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
    },
});
