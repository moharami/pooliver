import { StyleSheet, Dimensions } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'flex-end',
        position: 'relative',
        zIndex: 0,
        backgroundColor: 'rgb(246, 250, 253)'

    },
    image: {
        width: '100%',
        height: '25%',
        // height: '36%'
        // resizeMode: 'contain',
        // backgroundColor: 'rgb(61, 99, 223)',
    },
    headerText: {
        fontSize: 17,
        color: 'rgba(255, 255, 255, .5)',
        fontFamily: 'IRANSansMobile(FaNum)'
    },

    imageRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        paddingTop: 20
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        width: 200,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: '20%'
    },
    name: {
        fontSize: 15,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10,
        paddingLeft: 20
    },
    infoLabel: {
        fontSize: 13,
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        // paddingTop: 10,
        // paddingLeft: 20
    },
    scroll: {
        position: 'absolute',
        flex: 1,
        top: 60,
        right: 0,
        left: 0,
        bottom: 0,
        zIndex: 5,
        marginBottom: 60
    },
    body : {
        // paddingTop: '38%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10
    },


    walletTitle: {
        fontSize: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    },

    text: {
        fontSize: 8,
        // color: 'rgba(0, 125, 255, .9)',
        color: 'rgba(122, 130, 153, 1)',
        paddingTop: 8,
        // paddingBottom: 4,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center'
    },
    btnContainer: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    advertise: {
        width: '44%',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'rgba(255, 193, 39, 1)',
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 6,
        paddingBottom: 6,
        borderRadius: 5
    },
    buttonTitle: {
        fontSize: 11,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    slideContainers: {
        flexDirection: 'row',
        transform: [
            {rotateY: '180deg'},
        ],
    },
    navitemContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        // paddingTop: 15,
        paddingBottom: 30
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 100,
        height: 100,
        // paddingBottom: 40,
        // marginLeft: 20,
        padding: 5,
        marginLeft: 10,
        borderRadius: 5

    },
    navitemContainer2: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        // paddingTop: 15,
        paddingBottom: 30,
        paddingTop: 30
    },
    navContainer2: {
        alignItems: "center",
        justifyContent: 'center',
        width: 80,
        height: 80,
        // paddingBottom: 40,
        // marginLeft: 20,
        padding: 5,
        marginLeft: 10,
        borderRadius: 100,
        backgroundColor: 'rgb(13, 76, 241)',
        borderWidth: 5,
        borderColor: 'rgb(107, 148, 255)'
    },
    largeText: {
        fontSize: 60,
        color: 'lightgray',
        fontFamily: 'IRANSansMobile(FaNum)',
        position: 'absolute',
        top: 10,
        zIndex: 50
    },
    body2 : {
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingRight: 30,
        paddingLeft: 30,
        paddingBottom: 40,
        paddingTop: 2,
        backgroundColor: 'rgb(245, 245, 245)'
    },
    // row: {
    //     width: '100%',
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     justifyContent: 'space-between',
    //     height: 130
    // },
    // lastrow: {
    //     width: '100%',
    //     height: 130,
    //     flexDirection: 'row',
    //     alignItems: 'center',
    //     justifyContent: 'center',
    // },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    label: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 7,
        color: 'black'
    },
    bodyLabel: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingTop: 40,
        paddingBottom: 20,
        color: 'black'
    },
    newContainer: {
        width: 34,
        backgroundColor: 'rgb(247, 148, 29)',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 23,
        left: 15
    },
    newText: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        color: 'white'
    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    lastrow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    item: {
        width: '33%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15
    },
    bodyImage: {
        width: 30,
        resizeMode: 'contain',
        tintColor: 'white'
    },
    bodyyImage: {
        width: 30,
        resizeMode: 'contain',
        tintColor: 'white'
    },
    imageContainer: {
        width: 70,
        height: 70,
        backgroundColor: 'rgb(37, 133, 228)',
        alignItems: 'center',
        justifyContent: 'center',
        // padding: 20,
        borderRadius: 20,
        // elevation: 8,
        // borderWidth: 2,
        // borderTopColor: '#38d0e6',
        // borderRightColor: '#38d0e6',
        // borderLeftColor: '#24d1ba',
        // borderBottomColor: '#24d1ba',
    },
    // item: {
    //     width: '32%',
    //     height: '55%',
    //     // backgroundColor: 'red'
    //     // borderRadius: 15,
    // },
    reminderItem: {
        width: '85%',
        backgroundColor: 'rgb(233, 233, 233)',
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        paddingBottom: 7

    },
    // bodyImage: {
    //     width: 50,
    //     resizeMode: 'contain',
    //     tintColor: 'rgb(82, 82, 82)'
    // },
    // bodyyImage: {
    //     width: 50,
    //     resizeMode: 'contain',
    //     tintColor: 'rgb(82, 82, 82)'
    // },

});
