import React, {Component} from 'react';
import { View, ImageBackground, Text, BackHandler, Dimensions, ScrollView, Image, TouchableOpacity, ActivityIndicator} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
import Axios from 'axios';
export const url = 'http://172.20.21.25:8009/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import ProfileItem from "../../components/profileItem/index";
import FIcon from 'react-native-vector-icons/dist/Feather';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import IIcon from 'react-native-vector-icons/dist/Ionicons';
import FooterMenu from "../../components/footerMenu/index";
import {store} from '../../config/store';
import {connect} from 'react-redux';
import AlertView from '../../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient'
import hd from '../../assets/hd3.png'
import slideBG from '../../assets/slideBG.png'
import slideBGLeft from '../../assets/leftslideBG.png'
import Slideshow from 'react-native-image-slider-show';

import bime1 from "../../assets/newInsurance/body.png";
import bime2 from "../../assets/newInsurance/third.png";
import bime3 from "../../assets/newInsurance/travel.png";
import bime4 from "../../assets/newInsurance/medical.png";
import bime5 from "../../assets/newInsurance/fire.png";
import bime6 from "../../assets/newInsurance/life.png";
import bime7 from "../../assets/newInsurance/indi.png";
import bime10 from "../../assets/earth.png";
import mass from "../../assets/mssoliat.png";
import SoonModal from './soonModal'

class Mainpage extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            loading: true,
            signed: false,
            showPicker:false,
            loading2: false,
            modalVisible: false,
            modalVisible2: false,
            visibleinput2: false,
            visibleinput3: false,
            homeActive: false,
            insBuy: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress(){
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
        if(this.state.add){
            Actions.reset('reminders', {openDrawer: this.props.openDrawer});
        }
    }
    closeModal2() {
        this.setState({modalVisible2: false});
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    test() {
        if( this.state.province !== 0 && this.state.town !== 0  && this.state.timeIn.value !== 0 && this.state.type.value !== 0  && this.state.selectedStartDate !== null && this.state.mobile !== '') {
            this.addRemind();
        }
        else if(/^[-]?\d+$/.test(this.state.fname) === true) {
            this.setState({modalVisible: true, fillFname: true});
        }
        else if(/^[-]?\d+$/.test(this.state.lname) === true) {
            this.setState({modalVisible: true, fillLname: true});
        }
        else {
            // Alert.alert('','لطفا تمام موارد را پر نمایید');
            this.setState({modalVisible: true, fillAll: true});
        }
    }
    render() {
        const {user} = this.props;
        // if(this.state.loading){
        //     return (<Loader />)
        // }
        // else
        return (
            <View style={styles.container}>
                {/*<ImageBackground source={hd} style={{flex:1 , width: '100%', height: undefined,  aspectRatio: 1,}}>*/}
                {/*</ImageBackground>*/}
                <View style={{position: 'absolute', top: -3, left: 0, right: 0,  width: '100%',  zIndex: 10, height: 155}}>
                    <Image
                        style={{flex:1, height: undefined, width: undefined,}}
                        source={hd}
                        resizeMode="contain"
                    />
                </View>
                <View style={{position: 'absolute', top: 0, left: 0, right: 0, paddingRight: 10, paddingLeft: 10,  width: '100%',  zIndex: 20, height: 60, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                    <FIcon name="chevron-left" size={28} color="white" />
                    <Text style={styles.headerText}>pooliver</Text>
                    <Icon name="bars" size={20} color="white" />
                </View>
                <ScrollView style={styles.scroll} nestedScrollEnabled={true}>
                    <View style={styles.body}>
                        <ScrollView style={{ transform: [
                            { scaleX: -1},
                            // {  rotate: '180deg'},

                        ],}}
                                    horizontal={true} showsHorizontalScrollIndicator={false}
                        >
                            <View style={styles.navitemContainer}>
                                {/*{this.props.posts.category.map((item)=>*/}
                                    {/*<TouchableOpacity onPress={() => this.handlePress(item.id)} key={item.id}>*/}
                                        {/*<View style={[styles.navContainer, { borderColor: !this.state.homeActive && this.state.active === item.id ? 'rgb(240, 122, 133)': 'lightgray', borderWidth: 1}]}>*/}
                                            {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50}} />*/}
                                            {/*/!*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*!/*/}
                                            {/*<Text style={[styles.text, {color: !this.state.homeActive && this.state.active === item.id ? 'rgb(240, 122, 133)': 'gray'}]}>{item.title}</Text>*/}
                                        {/*</View>*/}
                                    {/*</TouchableOpacity>*/}
                                {/*)}*/}
                                <TouchableOpacity onPress={() => this.handlePress('home')}>
                                    <View style={[styles.navContainer, { backgroundColor: this.state.homeActive ? 'rgb(35, 81, 213)': 'white'}]}>
                                        <Image source={{uri: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEX///8jHyAAAADv7+8fGxwOBggeGRsyLzCcm5w+OzwMAAVBPz+ko6MaFRYJAAAcGBkUDhBxb2/29vbd3d2zsrLIx8fr6+u6ubmioaHS0tJ+fX2WlZVpZ2dHREUpJSbh4OBWVFW/vr/KycmPjo44NTaKiYlcWlpZV1hsamssKSpNS0t6eHirqqtiX2DSK2VdAAAOdElEQVR4nN1d54KiMBBeRrCtgYgde1t3Le//egcKZhICUgPc9+tOXchHpidMvr5KR2s0GXePzu9u3Tt1Ntqmc+qtd7/OcT6ejFrl375UjGbHR68NAKZBKdF1XXvB/Reh1DDdb/Te4zgbVT3QLNjOzleXgEECVlHQieH+7nqebasecgqM5g8dgFofuGFYFEB7zJswmfb+piWYOfls9mHjTOyqKcShdb+4c5eF3ZulO5c/45raH3t8AZPkYBeAmPAzq99MLoYARdDzSQLcFlVTwrDvpwLpvUChd6+aV4CRA2Ye3YtC24RzHVzI4gfop7HqFqFG33PwAcy+Qclnd0JhVbUDWVzjxdOinj/Xrr9O9z6eHUYvHGbje9f5vepeTEDbcRcgcKlSIV1+0fOgUxPo2unuF9F20V7su86aghnjYgjsquI4ukTy89xazxkn1aLt+HaKcaQEHlXoY2sYJZ+uP+ucU4cmrcn5D/oRj4yAo9xBdiPsi0tvN8j6xLeDXVTQQPtqfcfiD2TDsAAe43wP2wv8TKntgZ46s2oPQaYyFNY56b3QuvekAmKBU8DVk2Asu78OpED3PHIMWRBh0H1ht4iG/SMRUAKnccG3uf9JLLUOw2JvI8FeYglcj3Uo4VYTmbel7TJuheCEJ7BEj3yQcNThWNLdPExPhnjDNlzLjDgm32GjBtfSMuRx6Inq8F2y0HztOyGxIXRSzr3CEtonBdsXKQamKDk6LEu4j301QwpYpkbgW59DwgOrwu+y3YhOEK7Twu8ShVFPlB+jV3CgehCfIjHVxond0ABooUHcTHyEsFNd8dt+C2PQoUAjPhAubkEVRaIltAWKhcVwS4Fgv1dNgWgh2gIoyJSfBYLKQvwwVuJQBkVcVSDYhlkRV82IgRDiwDz/NQU/T7VqS5iLPimYokDQ3FW9oNA60UIpHnmCcCtmmLkgJKj57LpgRaFb1ChzQZCrPIZhLFxKRZydBOKDz+z6D8KFVBRJkoEPQXTIaP22wmVKSsoy4c6Nra1nsn+21uYI1mrFUlAgss5yjTWpMUExGTAy1OAcLuGtHUGRYvr4jZeCWulgAF4X084Bb2Vq4yZ4cOGIbqWzNn84o66Jow9jiDWJXtL86Q1Xt8w6hGpyXHGMmiZC5ZSY/pQ3wtzocLKWuHLTwgTbf2WOMC+mOF+0Okn/7Io9YdaISBG4yNI8J/sjzgrXKBiVo8uNNpHL4GRUVVk7Bx7I2uiJVOqBZJRcyx5fAdCQKpoJZmTPTWFNd3pyWHAj/mw2+uiJVFpWS44jcvzWxyzjiHw9fagYXwE4tZPPyhTNuE6rrqslxQiPWov/LTYzDZFRD1hOzdjVU+w/SZ2jNREbbD3izOM3CvPMJtjRANie0phMAXuKIlYEFGKI/D5EL04jm6QnjmLrARtPYmTRhpvCOtYt4tBFxibS7f8xdSU7laMrBCh4i5pEbgqr3iKfHrh2FqGJV+vjQ6g1kBWh0mXqxednUG9gGTRl4RgKZxo5hV9fPTaJpsTX4Yi05pWLKKBJ1Gn46zNzmeRX/egKQYeZU0n1Bb322UBD+gIyp2F3N4v7sjFAyW1I05CraFw4w7BkFA0hiUJ2pmkRKQamIWTCXUS+WUkFjxVzeULtFLkSaErtQoYJm0Q+rkGFDtKU8pMcKE0k+HOkoQ22Mx5QrRDwawTMVepWZYMrBFu5mEZ83EiwUpOOxHTQj7JAzQPyCojLjtlYo8LBFQIkj8zpoypOXCWuIei8HZ/1XjlDTqThltTDkTmMt2tHiVO/ye7+BVSreKdQLKBpcFrBwOaL+uv6aFm7X8ju/orByjHt79cnWA2bmvti3Jnv8wtSKGTrVzy4QoCCbN8jsllteNQdgNX3/UwQfVDXLXrp8MOm7FlTQ3kxlP1SrxowtdM33v+xocm6KDoZ3J8YRFkqe+oj/jrBr/J5ZVQ3ffr8rsA4CzQwn4AoRV763ZM+3IH4P8unLVPB1AwFqc0AtvwfJQX+Y9R78Rfy89S8bpklEqa3u/n7HdGIBbjEYPWfqEuoZchKo9TbBRZbCU8EFBRFbZ1Ty9B5B26e/2vlj2gGeIFZnpyoZcgyeu+GC8HwZMAJbWaJCBrUMkTuoY1Na1ZTyu0NjLA1ahmOOAfI4lQr42bSG/dSpzwuUsvQ5jSPpcQZnYXtPyIrThLUMsTW84DsDk24D1yAv2hnBQG81NYoZsg8oOsfmC/LeN31a/Jg4YcOUlujmCGLvfv3r+t7Rs1Mbzf51TtXOAMLJtvIoZghi9Po8uvERDaTw/fLWJ6IW6/xyeIaxQw51dswhpnK3T4tL1rwycpsjWKGLH+izpfGGGYJaXx3+lw4DtyQxNYoZsiCGnLDDLPsovEXBPrPcoFflpTYGsUM7+84kqwwwwxbvaacdQkeXfhKihmO3wytn5wMfYEnr7cbgyg+bGsUM2R7Z/R1Tin1d5AHr9H6fihsaxQzRDvyO/kszdsF+lnJPsrWKGaIX8DA3iI9Qz9SY3sZfT8UsjXVMdzk8oeB3rEqZOBqRZ1WzHCBGf7lYDh/2U60/WgRYWuqY9jBUVvq1VE/uafopT9/jLrO/1IxQ5Tkn77W78g7dVz6nrDBYeLjcJPnUIoZ7rG32GXPLd7JvcEa5gdBvWBrKvP47R3OD1M2XLK5+owI3tZUF5f+fjmsmpuyln4X231y4G1NlbkF26aQdjvUOvZQA34vuWKGN5wfsoUZkq7r6ShWSAVbo5ghW/Q15jgMT1dNDCa/TQT4pouzNYoZrt8LF+YMuY6UFeGA4HooICiBY1ujmCHaDzzh6sNpqvpBghKOhAKpwLamyopw1pWZ3UsQJK9OB9fHux8Dht/xFw0Y5mwAy1f1bbT3O0XYNo1Mdtkbq+h6b3v2HUaPKWwQ83UkP+skD7nw60Huf3vvoCbNLv1gt7EsbQ6iOWRr3gz1dgiUWbhgr7LkV2n6pjL/8NwVxSxrGofoJ/fyJg2dkK1B+1pDsMIMZUjBcMivxSx5wskQWGB5z+uADxNhtQzZguYzTkNvPCXfIPxehpGa30BJmRlSytBGm7o95cWGJ2ktqhXWNA4/Yg6llGGIEft/4vzpGCRLEfZtH3wfDH4JMWBOksb9LDFDlBLA8wO2gTaxqWkF+PSDwNTYrTiErytD4niExd2+eLCFmhSmps5gsu4v+o7RpDZ/mze/f+Yl2Vgxm79Vn9+452sJelM/21J+vcC07p0tsahGP1U6tmLA2nu8d5fM+6FpbTDQa0HviGuUwSPWF/g1tXcEwxKo/2Av+469nsc2Sg5Rjth0f4F8BQpgUPBd+16Xn4C9O/N9iHdje2IEuMjlkcmuvLNLc4B2sXM2BUfjzbamaL8yl4wgMf3cPLLWwA1cOGnE0tvM9jsvoMVfYZECdXZpdGyK9isLXgFVifV2RaMrADbugSl894vEtDldPUUgixmqV6BdRNaHxYUaA/XdCy9RoJaCje2rgHJfK1yQQSkUSXXKQI3wjVbVwm4d7zto6AvPuIMuSL5HhrahORTq5mXIOpdvmz6J3BRKaxU/DU+E1x97d3LNL5tnTrn2qxGRJ3oIDYy/Ucwd2aibE+SmBTaDRGYEGaPs73VXAxyRxlgRrInxzdtrB/wSZFz+hxpGNas4zPVkj+ugi3/YiPNJAuBTkeKnhmve3pyKzRJpoRGfwXPH6DTm7AAcjunkQ7EQN29vzPkP+EiHz5Kn62l+XQvgg1cTpO8TLKeNsKf8WUEJcoZf3AK0CcEbPp/kg5l5gduCn/Qkswqxwmd26Z9//yUc0Fn7+HTOjTZhGyiUKNa+BM6dnWck7WuJo1jN2tR5MaplIiVMkS1wZ6/RXXkDzI0Td4ZlirTd6aM/NOt7FsSOO4c01Ts//LOpayJ1w+qUsm8nPu2itrENfx7wp3hUxH9/pnPzzuXOcBoAd5pw7c9Wz7KFxN5YNabIa1HG+Jk7TdilWKfGmLwOtvWMUQnfpqxO26UG3Mj07JHlTKBYF6exFMaVQ4G6wqXqcXyJI4wqV/pzFC5Wh6b0F2FMOV/lO/OXM3dVZxrTE+UJ5m5BLlCkm2rzxQNYBRMMUbQqNalz0AsnGKKoQWXHmNgPcSg5dTCAYJw1s1eNpC50KhAszH0NBIpWUc8uFY6ChOpF6ovg+t2n96N6TWPbE9pwFBwpH0BogkH6agOcpWBDNUIKVpXthvB30GCnruS/6Ih9VIzvwh2zvRZvQlTVb2xHlCANSnmlwBGVUTM1FRXxOQgm1FXBkg5xGIu64N5qXXZiPPsLNfohpLRUddsJvZJtwaVMjvsehF7vhlJj41u47Q6BR1n7/CbfIQUsT0IDzEC0qR7HnzLmcS/hpxmb0utFrUt4Gl2OvYJtjj3QQlrvTaCSBHUcsm1PfdSPxbngxQ1MSXsFo62oGmavwurvNfeC66wIGzAddGTP0H2IChekDxtprzYLzFVOkq37Tjp9nmNSm9EspY/Z00h43LOGc6PuGsyw9j0FlCov9E1/JVb1RdKE0/GQdipbe2cDhpyeRuFcRX1ocZVYO1+kDID1ebZNNix7NL51AGhU6xYCw6r29RzWkRxdpaQAxunWnYyiebYWs+XqzwQzkl2pEUUSuBwjZNWfTOp1/tzsfs/z+3h/GD2xmMzug66zunrHOxkkpuuOJ5+V8vOw2EXYHMzTItQwTdRNx+wblMR2lnzCgGEdNruMbtCPnYeMsACOddlXZ8878cKaARTWdVkIeuGwivJkWUDAdKpWvzBsLxopgqRrf1b1WarkMb1f4bPdiYMb3OYO/EpGaz8krgfIxM4NhjrnOq2lR2LRvbiKRNNILHHDg83vvQ6uISlGY2ftuj0a78+9d+OJFxNcjvu6OIZUGI2Pl5MXuJgGdbm+yeouL+rGAO432vdquW/S1Elgtxb7cffoBmnr3ulvo206p956Nzwf5+O4kLUw/AMWp9YTlYNIOAAAAABJRU5ErkJggg==" }} style={{ width: 30, height: 30, tintColor: this.state.homeActive ? 'white' : 'gray'}} />
                                        {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*/}
                                        <Text style={[styles.text, {color: this.state.homeActive ? 'white': 'gray'}]}>همه مقالات همه مقالات همه مقالات</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.handlePress('home')}>
                                    <View style={[styles.navContainer, { backgroundColor: this.state.homeActive ? 'rgb(35, 81, 213)': 'white'}]}>
                                        <Image source={{uri: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEX///8jHyAAAADv7+8fGxwOBggeGRsyLzCcm5w+OzwMAAVBPz+ko6MaFRYJAAAcGBkUDhBxb2/29vbd3d2zsrLIx8fr6+u6ubmioaHS0tJ+fX2WlZVpZ2dHREUpJSbh4OBWVFW/vr/KycmPjo44NTaKiYlcWlpZV1hsamssKSpNS0t6eHirqqtiX2DSK2VdAAAOdElEQVR4nN1d54KiMBBeRrCtgYgde1t3Le//egcKZhICUgPc9+tOXchHpidMvr5KR2s0GXePzu9u3Tt1Ntqmc+qtd7/OcT6ejFrl375UjGbHR68NAKZBKdF1XXvB/Reh1DDdb/Te4zgbVT3QLNjOzleXgEECVlHQieH+7nqebasecgqM5g8dgFofuGFYFEB7zJswmfb+piWYOfls9mHjTOyqKcShdb+4c5eF3ZulO5c/45raH3t8AZPkYBeAmPAzq99MLoYARdDzSQLcFlVTwrDvpwLpvUChd6+aV4CRA2Ye3YtC24RzHVzI4gfop7HqFqFG33PwAcy+Qclnd0JhVbUDWVzjxdOinj/Xrr9O9z6eHUYvHGbje9f5vepeTEDbcRcgcKlSIV1+0fOgUxPo2unuF9F20V7su86aghnjYgjsquI4ukTy89xazxkn1aLt+HaKcaQEHlXoY2sYJZ+uP+ucU4cmrcn5D/oRj4yAo9xBdiPsi0tvN8j6xLeDXVTQQPtqfcfiD2TDsAAe43wP2wv8TKntgZ46s2oPQaYyFNY56b3QuvekAmKBU8DVk2Asu78OpED3PHIMWRBh0H1ht4iG/SMRUAKnccG3uf9JLLUOw2JvI8FeYglcj3Uo4VYTmbel7TJuheCEJ7BEj3yQcNThWNLdPExPhnjDNlzLjDgm32GjBtfSMuRx6Inq8F2y0HztOyGxIXRSzr3CEtonBdsXKQamKDk6LEu4j301QwpYpkbgW59DwgOrwu+y3YhOEK7Twu8ShVFPlB+jV3CgehCfIjHVxond0ABooUHcTHyEsFNd8dt+C2PQoUAjPhAubkEVRaIltAWKhcVwS4Fgv1dNgWgh2gIoyJSfBYLKQvwwVuJQBkVcVSDYhlkRV82IgRDiwDz/NQU/T7VqS5iLPimYokDQ3FW9oNA60UIpHnmCcCtmmLkgJKj57LpgRaFb1ChzQZCrPIZhLFxKRZydBOKDz+z6D8KFVBRJkoEPQXTIaP22wmVKSsoy4c6Nra1nsn+21uYI1mrFUlAgss5yjTWpMUExGTAy1OAcLuGtHUGRYvr4jZeCWulgAF4X084Bb2Vq4yZ4cOGIbqWzNn84o66Jow9jiDWJXtL86Q1Xt8w6hGpyXHGMmiZC5ZSY/pQ3wtzocLKWuHLTwgTbf2WOMC+mOF+0Okn/7Io9YdaISBG4yNI8J/sjzgrXKBiVo8uNNpHL4GRUVVk7Bx7I2uiJVOqBZJRcyx5fAdCQKpoJZmTPTWFNd3pyWHAj/mw2+uiJVFpWS44jcvzWxyzjiHw9fagYXwE4tZPPyhTNuE6rrqslxQiPWov/LTYzDZFRD1hOzdjVU+w/SZ2jNREbbD3izOM3CvPMJtjRANie0phMAXuKIlYEFGKI/D5EL04jm6QnjmLrARtPYmTRhpvCOtYt4tBFxibS7f8xdSU7laMrBCh4i5pEbgqr3iKfHrh2FqGJV+vjQ6g1kBWh0mXqxednUG9gGTRl4RgKZxo5hV9fPTaJpsTX4Yi05pWLKKBJ1Gn46zNzmeRX/egKQYeZU0n1Bb322UBD+gIyp2F3N4v7sjFAyW1I05CraFw4w7BkFA0hiUJ2pmkRKQamIWTCXUS+WUkFjxVzeULtFLkSaErtQoYJm0Q+rkGFDtKU8pMcKE0k+HOkoQ22Mx5QrRDwawTMVepWZYMrBFu5mEZ83EiwUpOOxHTQj7JAzQPyCojLjtlYo8LBFQIkj8zpoypOXCWuIei8HZ/1XjlDTqThltTDkTmMt2tHiVO/ye7+BVSreKdQLKBpcFrBwOaL+uv6aFm7X8ju/orByjHt79cnWA2bmvti3Jnv8wtSKGTrVzy4QoCCbN8jsllteNQdgNX3/UwQfVDXLXrp8MOm7FlTQ3kxlP1SrxowtdM33v+xocm6KDoZ3J8YRFkqe+oj/jrBr/J5ZVQ3ffr8rsA4CzQwn4AoRV763ZM+3IH4P8unLVPB1AwFqc0AtvwfJQX+Y9R78Rfy89S8bpklEqa3u/n7HdGIBbjEYPWfqEuoZchKo9TbBRZbCU8EFBRFbZ1Ty9B5B26e/2vlj2gGeIFZnpyoZcgyeu+GC8HwZMAJbWaJCBrUMkTuoY1Na1ZTyu0NjLA1ahmOOAfI4lQr42bSG/dSpzwuUsvQ5jSPpcQZnYXtPyIrThLUMsTW84DsDk24D1yAv2hnBQG81NYoZsg8oOsfmC/LeN31a/Jg4YcOUlujmCGLvfv3r+t7Rs1Mbzf51TtXOAMLJtvIoZghi9Po8uvERDaTw/fLWJ6IW6/xyeIaxQw51dswhpnK3T4tL1rwycpsjWKGLH+izpfGGGYJaXx3+lw4DtyQxNYoZsiCGnLDDLPsovEXBPrPcoFflpTYGsUM7+84kqwwwwxbvaacdQkeXfhKihmO3wytn5wMfYEnr7cbgyg+bGsUM2R7Z/R1Tin1d5AHr9H6fihsaxQzRDvyO/kszdsF+lnJPsrWKGaIX8DA3iI9Qz9SY3sZfT8UsjXVMdzk8oeB3rEqZOBqRZ1WzHCBGf7lYDh/2U60/WgRYWuqY9jBUVvq1VE/uafopT9/jLrO/1IxQ5Tkn77W78g7dVz6nrDBYeLjcJPnUIoZ7rG32GXPLd7JvcEa5gdBvWBrKvP47R3OD1M2XLK5+owI3tZUF5f+fjmsmpuyln4X231y4G1NlbkF26aQdjvUOvZQA34vuWKGN5wfsoUZkq7r6ShWSAVbo5ghW/Q15jgMT1dNDCa/TQT4pouzNYoZrt8LF+YMuY6UFeGA4HooICiBY1ujmCHaDzzh6sNpqvpBghKOhAKpwLamyopw1pWZ3UsQJK9OB9fHux8Dht/xFw0Y5mwAy1f1bbT3O0XYNo1Mdtkbq+h6b3v2HUaPKWwQ83UkP+skD7nw60Huf3vvoCbNLv1gt7EsbQ6iOWRr3gz1dgiUWbhgr7LkV2n6pjL/8NwVxSxrGofoJ/fyJg2dkK1B+1pDsMIMZUjBcMivxSx5wskQWGB5z+uADxNhtQzZguYzTkNvPCXfIPxehpGa30BJmRlSytBGm7o95cWGJ2ktqhXWNA4/Yg6llGGIEft/4vzpGCRLEfZtH3wfDH4JMWBOksb9LDFDlBLA8wO2gTaxqWkF+PSDwNTYrTiErytD4niExd2+eLCFmhSmps5gsu4v+o7RpDZ/mze/f+Yl2Vgxm79Vn9+452sJelM/21J+vcC07p0tsahGP1U6tmLA2nu8d5fM+6FpbTDQa0HviGuUwSPWF/g1tXcEwxKo/2Av+469nsc2Sg5Rjth0f4F8BQpgUPBd+16Xn4C9O/N9iHdje2IEuMjlkcmuvLNLc4B2sXM2BUfjzbamaL8yl4wgMf3cPLLWwA1cOGnE0tvM9jsvoMVfYZECdXZpdGyK9isLXgFVifV2RaMrADbugSl894vEtDldPUUgixmqV6BdRNaHxYUaA/XdCy9RoJaCje2rgHJfK1yQQSkUSXXKQI3wjVbVwm4d7zto6AvPuIMuSL5HhrahORTq5mXIOpdvmz6J3BRKaxU/DU+E1x97d3LNL5tnTrn2qxGRJ3oIDYy/Ucwd2aibE+SmBTaDRGYEGaPs73VXAxyRxlgRrInxzdtrB/wSZFz+hxpGNas4zPVkj+ugi3/YiPNJAuBTkeKnhmve3pyKzRJpoRGfwXPH6DTm7AAcjunkQ7EQN29vzPkP+EiHz5Kn62l+XQvgg1cTpO8TLKeNsKf8WUEJcoZf3AK0CcEbPp/kg5l5gduCn/Qkswqxwmd26Z9//yUc0Fn7+HTOjTZhGyiUKNa+BM6dnWck7WuJo1jN2tR5MaplIiVMkS1wZ6/RXXkDzI0Td4ZlirTd6aM/NOt7FsSOO4c01Ts//LOpayJ1w+qUsm8nPu2itrENfx7wp3hUxH9/pnPzzuXOcBoAd5pw7c9Wz7KFxN5YNabIa1HG+Jk7TdilWKfGmLwOtvWMUQnfpqxO26UG3Mj07JHlTKBYF6exFMaVQ4G6wqXqcXyJI4wqV/pzFC5Wh6b0F2FMOV/lO/OXM3dVZxrTE+UJ5m5BLlCkm2rzxQNYBRMMUbQqNalz0AsnGKKoQWXHmNgPcSg5dTCAYJw1s1eNpC50KhAszH0NBIpWUc8uFY6ChOpF6ovg+t2n96N6TWPbE9pwFBwpH0BogkH6agOcpWBDNUIKVpXthvB30GCnruS/6Ih9VIzvwh2zvRZvQlTVb2xHlCANSnmlwBGVUTM1FRXxOQgm1FXBkg5xGIu64N5qXXZiPPsLNfohpLRUddsJvZJtwaVMjvsehF7vhlJj41u47Q6BR1n7/CbfIQUsT0IDzEC0qR7HnzLmcS/hpxmb0utFrUt4Gl2OvYJtjj3QQlrvTaCSBHUcsm1PfdSPxbngxQ1MSXsFo62oGmavwurvNfeC66wIGzAddGTP0H2IChekDxtprzYLzFVOkq37Tjp9nmNSm9EspY/Z00h43LOGc6PuGsyw9j0FlCov9E1/JVb1RdKE0/GQdipbe2cDhpyeRuFcRX1ocZVYO1+kDID1ebZNNix7NL51AGhU6xYCw6r29RzWkRxdpaQAxunWnYyiebYWs+XqzwQzkl2pEUUSuBwjZNWfTOp1/tzsfs/z+3h/GD2xmMzug66zunrHOxkkpuuOJ5+V8vOw2EXYHMzTItQwTdRNx+wblMR2lnzCgGEdNruMbtCPnYeMsACOddlXZ8878cKaARTWdVkIeuGwivJkWUDAdKpWvzBsLxopgqRrf1b1WarkMb1f4bPdiYMb3OYO/EpGaz8krgfIxM4NhjrnOq2lR2LRvbiKRNNILHHDg83vvQ6uISlGY2ftuj0a78+9d+OJFxNcjvu6OIZUGI2Pl5MXuJgGdbm+yeouL+rGAO432vdquW/S1Elgtxb7cffoBmnr3ulvo206p956Nzwf5+O4kLUw/AMWp9YTlYNIOAAAAABJRU5ErkJggg==" }} style={{ width: 30, height: 30, tintColor: this.state.homeActive ? 'white' : 'gray'}} />
                                        {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*/}
                                        <Text style={[styles.text, {color: this.state.homeActive ? 'white': 'gray'}]}>همه مقالات همه مقالات همه مقالات</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.handlePress('home')}>
                                    <View style={[styles.navContainer, { backgroundColor: this.state.homeActive ? 'rgb(35, 81, 213)': 'white'}]}>
                                        <Image source={{uri: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEX///8jHyAAAADv7+8fGxwOBggeGRsyLzCcm5w+OzwMAAVBPz+ko6MaFRYJAAAcGBkUDhBxb2/29vbd3d2zsrLIx8fr6+u6ubmioaHS0tJ+fX2WlZVpZ2dHREUpJSbh4OBWVFW/vr/KycmPjo44NTaKiYlcWlpZV1hsamssKSpNS0t6eHirqqtiX2DSK2VdAAAOdElEQVR4nN1d54KiMBBeRrCtgYgde1t3Le//egcKZhICUgPc9+tOXchHpidMvr5KR2s0GXePzu9u3Tt1Ntqmc+qtd7/OcT6ejFrl375UjGbHR68NAKZBKdF1XXvB/Reh1DDdb/Te4zgbVT3QLNjOzleXgEECVlHQieH+7nqebasecgqM5g8dgFofuGFYFEB7zJswmfb+piWYOfls9mHjTOyqKcShdb+4c5eF3ZulO5c/45raH3t8AZPkYBeAmPAzq99MLoYARdDzSQLcFlVTwrDvpwLpvUChd6+aV4CRA2Ye3YtC24RzHVzI4gfop7HqFqFG33PwAcy+Qclnd0JhVbUDWVzjxdOinj/Xrr9O9z6eHUYvHGbje9f5vepeTEDbcRcgcKlSIV1+0fOgUxPo2unuF9F20V7su86aghnjYgjsquI4ukTy89xazxkn1aLt+HaKcaQEHlXoY2sYJZ+uP+ucU4cmrcn5D/oRj4yAo9xBdiPsi0tvN8j6xLeDXVTQQPtqfcfiD2TDsAAe43wP2wv8TKntgZ46s2oPQaYyFNY56b3QuvekAmKBU8DVk2Asu78OpED3PHIMWRBh0H1ht4iG/SMRUAKnccG3uf9JLLUOw2JvI8FeYglcj3Uo4VYTmbel7TJuheCEJ7BEj3yQcNThWNLdPExPhnjDNlzLjDgm32GjBtfSMuRx6Inq8F2y0HztOyGxIXRSzr3CEtonBdsXKQamKDk6LEu4j301QwpYpkbgW59DwgOrwu+y3YhOEK7Twu8ShVFPlB+jV3CgehCfIjHVxond0ABooUHcTHyEsFNd8dt+C2PQoUAjPhAubkEVRaIltAWKhcVwS4Fgv1dNgWgh2gIoyJSfBYLKQvwwVuJQBkVcVSDYhlkRV82IgRDiwDz/NQU/T7VqS5iLPimYokDQ3FW9oNA60UIpHnmCcCtmmLkgJKj57LpgRaFb1ChzQZCrPIZhLFxKRZydBOKDz+z6D8KFVBRJkoEPQXTIaP22wmVKSsoy4c6Nra1nsn+21uYI1mrFUlAgss5yjTWpMUExGTAy1OAcLuGtHUGRYvr4jZeCWulgAF4X084Bb2Vq4yZ4cOGIbqWzNn84o66Jow9jiDWJXtL86Q1Xt8w6hGpyXHGMmiZC5ZSY/pQ3wtzocLKWuHLTwgTbf2WOMC+mOF+0Okn/7Io9YdaISBG4yNI8J/sjzgrXKBiVo8uNNpHL4GRUVVk7Bx7I2uiJVOqBZJRcyx5fAdCQKpoJZmTPTWFNd3pyWHAj/mw2+uiJVFpWS44jcvzWxyzjiHw9fagYXwE4tZPPyhTNuE6rrqslxQiPWov/LTYzDZFRD1hOzdjVU+w/SZ2jNREbbD3izOM3CvPMJtjRANie0phMAXuKIlYEFGKI/D5EL04jm6QnjmLrARtPYmTRhpvCOtYt4tBFxibS7f8xdSU7laMrBCh4i5pEbgqr3iKfHrh2FqGJV+vjQ6g1kBWh0mXqxednUG9gGTRl4RgKZxo5hV9fPTaJpsTX4Yi05pWLKKBJ1Gn46zNzmeRX/egKQYeZU0n1Bb322UBD+gIyp2F3N4v7sjFAyW1I05CraFw4w7BkFA0hiUJ2pmkRKQamIWTCXUS+WUkFjxVzeULtFLkSaErtQoYJm0Q+rkGFDtKU8pMcKE0k+HOkoQ22Mx5QrRDwawTMVepWZYMrBFu5mEZ83EiwUpOOxHTQj7JAzQPyCojLjtlYo8LBFQIkj8zpoypOXCWuIei8HZ/1XjlDTqThltTDkTmMt2tHiVO/ye7+BVSreKdQLKBpcFrBwOaL+uv6aFm7X8ju/orByjHt79cnWA2bmvti3Jnv8wtSKGTrVzy4QoCCbN8jsllteNQdgNX3/UwQfVDXLXrp8MOm7FlTQ3kxlP1SrxowtdM33v+xocm6KDoZ3J8YRFkqe+oj/jrBr/J5ZVQ3ffr8rsA4CzQwn4AoRV763ZM+3IH4P8unLVPB1AwFqc0AtvwfJQX+Y9R78Rfy89S8bpklEqa3u/n7HdGIBbjEYPWfqEuoZchKo9TbBRZbCU8EFBRFbZ1Ty9B5B26e/2vlj2gGeIFZnpyoZcgyeu+GC8HwZMAJbWaJCBrUMkTuoY1Na1ZTyu0NjLA1ahmOOAfI4lQr42bSG/dSpzwuUsvQ5jSPpcQZnYXtPyIrThLUMsTW84DsDk24D1yAv2hnBQG81NYoZsg8oOsfmC/LeN31a/Jg4YcOUlujmCGLvfv3r+t7Rs1Mbzf51TtXOAMLJtvIoZghi9Po8uvERDaTw/fLWJ6IW6/xyeIaxQw51dswhpnK3T4tL1rwycpsjWKGLH+izpfGGGYJaXx3+lw4DtyQxNYoZsiCGnLDDLPsovEXBPrPcoFflpTYGsUM7+84kqwwwwxbvaacdQkeXfhKihmO3wytn5wMfYEnr7cbgyg+bGsUM2R7Z/R1Tin1d5AHr9H6fihsaxQzRDvyO/kszdsF+lnJPsrWKGaIX8DA3iI9Qz9SY3sZfT8UsjXVMdzk8oeB3rEqZOBqRZ1WzHCBGf7lYDh/2U60/WgRYWuqY9jBUVvq1VE/uafopT9/jLrO/1IxQ5Tkn77W78g7dVz6nrDBYeLjcJPnUIoZ7rG32GXPLd7JvcEa5gdBvWBrKvP47R3OD1M2XLK5+owI3tZUF5f+fjmsmpuyln4X231y4G1NlbkF26aQdjvUOvZQA34vuWKGN5wfsoUZkq7r6ShWSAVbo5ghW/Q15jgMT1dNDCa/TQT4pouzNYoZrt8LF+YMuY6UFeGA4HooICiBY1ujmCHaDzzh6sNpqvpBghKOhAKpwLamyopw1pWZ3UsQJK9OB9fHux8Dht/xFw0Y5mwAy1f1bbT3O0XYNo1Mdtkbq+h6b3v2HUaPKWwQ83UkP+skD7nw60Huf3vvoCbNLv1gt7EsbQ6iOWRr3gz1dgiUWbhgr7LkV2n6pjL/8NwVxSxrGofoJ/fyJg2dkK1B+1pDsMIMZUjBcMivxSx5wskQWGB5z+uADxNhtQzZguYzTkNvPCXfIPxehpGa30BJmRlSytBGm7o95cWGJ2ktqhXWNA4/Yg6llGGIEft/4vzpGCRLEfZtH3wfDH4JMWBOksb9LDFDlBLA8wO2gTaxqWkF+PSDwNTYrTiErytD4niExd2+eLCFmhSmps5gsu4v+o7RpDZ/mze/f+Yl2Vgxm79Vn9+452sJelM/21J+vcC07p0tsahGP1U6tmLA2nu8d5fM+6FpbTDQa0HviGuUwSPWF/g1tXcEwxKo/2Av+469nsc2Sg5Rjth0f4F8BQpgUPBd+16Xn4C9O/N9iHdje2IEuMjlkcmuvLNLc4B2sXM2BUfjzbamaL8yl4wgMf3cPLLWwA1cOGnE0tvM9jsvoMVfYZECdXZpdGyK9isLXgFVifV2RaMrADbugSl894vEtDldPUUgixmqV6BdRNaHxYUaA/XdCy9RoJaCje2rgHJfK1yQQSkUSXXKQI3wjVbVwm4d7zto6AvPuIMuSL5HhrahORTq5mXIOpdvmz6J3BRKaxU/DU+E1x97d3LNL5tnTrn2qxGRJ3oIDYy/Ucwd2aibE+SmBTaDRGYEGaPs73VXAxyRxlgRrInxzdtrB/wSZFz+hxpGNas4zPVkj+ugi3/YiPNJAuBTkeKnhmve3pyKzRJpoRGfwXPH6DTm7AAcjunkQ7EQN29vzPkP+EiHz5Kn62l+XQvgg1cTpO8TLKeNsKf8WUEJcoZf3AK0CcEbPp/kg5l5gduCn/Qkswqxwmd26Z9//yUc0Fn7+HTOjTZhGyiUKNa+BM6dnWck7WuJo1jN2tR5MaplIiVMkS1wZ6/RXXkDzI0Td4ZlirTd6aM/NOt7FsSOO4c01Ts//LOpayJ1w+qUsm8nPu2itrENfx7wp3hUxH9/pnPzzuXOcBoAd5pw7c9Wz7KFxN5YNabIa1HG+Jk7TdilWKfGmLwOtvWMUQnfpqxO26UG3Mj07JHlTKBYF6exFMaVQ4G6wqXqcXyJI4wqV/pzFC5Wh6b0F2FMOV/lO/OXM3dVZxrTE+UJ5m5BLlCkm2rzxQNYBRMMUbQqNalz0AsnGKKoQWXHmNgPcSg5dTCAYJw1s1eNpC50KhAszH0NBIpWUc8uFY6ChOpF6ovg+t2n96N6TWPbE9pwFBwpH0BogkH6agOcpWBDNUIKVpXthvB30GCnruS/6Ih9VIzvwh2zvRZvQlTVb2xHlCANSnmlwBGVUTM1FRXxOQgm1FXBkg5xGIu64N5qXXZiPPsLNfohpLRUddsJvZJtwaVMjvsehF7vhlJj41u47Q6BR1n7/CbfIQUsT0IDzEC0qR7HnzLmcS/hpxmb0utFrUt4Gl2OvYJtjj3QQlrvTaCSBHUcsm1PfdSPxbngxQ1MSXsFo62oGmavwurvNfeC66wIGzAddGTP0H2IChekDxtprzYLzFVOkq37Tjp9nmNSm9EspY/Z00h43LOGc6PuGsyw9j0FlCov9E1/JVb1RdKE0/GQdipbe2cDhpyeRuFcRX1ocZVYO1+kDID1ebZNNix7NL51AGhU6xYCw6r29RzWkRxdpaQAxunWnYyiebYWs+XqzwQzkl2pEUUSuBwjZNWfTOp1/tzsfs/z+3h/GD2xmMzug66zunrHOxkkpuuOJ5+V8vOw2EXYHMzTItQwTdRNx+wblMR2lnzCgGEdNruMbtCPnYeMsACOddlXZ8878cKaARTWdVkIeuGwivJkWUDAdKpWvzBsLxopgqRrf1b1WarkMb1f4bPdiYMb3OYO/EpGaz8krgfIxM4NhjrnOq2lR2LRvbiKRNNILHHDg83vvQ6uISlGY2ftuj0a78+9d+OJFxNcjvu6OIZUGI2Pl5MXuJgGdbm+yeouL+rGAO432vdquW/S1Elgtxb7cffoBmnr3ulvo206p956Nzwf5+O4kLUw/AMWp9YTlYNIOAAAAABJRU5ErkJggg==" }} style={{ width: 30, height: 30, tintColor: this.state.homeActive ? 'white' : 'gray'}} />
                                        {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*/}
                                        <Text style={[styles.text, {color: this.state.homeActive ? 'white': 'gray'}]}>همه مقالات همه مقالات همه مقالات</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                        <View style={styles.btnContainer}>
                            <LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(246, 94, 23)', 'rgb(255, 150, 35)']} style={[styles.advertise, {  marginRight: 5}]}>
                                <TouchableOpacity>
                                    <Text style={[styles.buttonTitle, {fontSize: 11}]}>عضویت در سیستم بازاریابی</Text>
                                </TouchableOpacity>
                            </LinearGradient>
                            <LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(88, 230, 210)', 'rgb(98, 204, 206)']} style={[styles.advertise, {  marginLeft: 5}]}>
                                <TouchableOpacity>
                                    <Text style={styles.buttonTitle}>خرید آنلاین انواع بیمه</Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>
                        <Icon name="chevron-down" size={20} color="rgb(246, 94, 23)" style={{paddingRight: '45%'}} />
                        {
                            this.state.insBuy ?
                                <View style={{width: '100%'}}>
                                    <Text style={[styles.text, {fontSize: 12, color: 'black'}]}>ورود اطلاعات اولیه و شروع روند عضویت در سیستم</Text>
                                    <Text style={[styles.text, {color: 'rgb(90, 90, 90)', fontSize: 10, lineHeight: 18, padding: 5}]}>ورود اطلاعات اولیه و شروع روند عضویت در سیستم ورود اطلاعات اولیه و شروع روند عضویت در سیستم ورود اطلاعات اولیه و شروع روند عضویت در سیستم</Text>
                                    {/*<View style={{backgroundColor: 'red', width: '100%'}}>*/}
                                    {/*<Image source={slideBG} style={{width: '100%', height: 250, resizeMode: 'contain'}} />*/}
                                    {/*</View>*/}
                                    <View style={{ width: '100%', position: 'relative', zIndex: 0, height: 250, marginTop: 40}}>
                                        <Image source={slideBG} style={{width: '100%', height: 250, resizeMode: 'contain',  position: 'absolute', right: 0, top: 0, zIndex: 10}} />
                                        <Text style={[styles.largeText, {left: 30}]}>01</Text>
                                        <Slideshow leftt={10} rightt={0}
                                                   dataSource={[
                                                       { url:'http://placeimg.com/640/480/any' },
                                                       { url:'http://placeimg.com/640/480/any' },
                                                       { url:'http://placeimg.com/640/480/any' }
                                                   ]} arrowSize={0} containerStyle={{paddingBottom: -30,  position: 'absolute', right: 5, top: 0, zIndex: 5}} height={250}  />
                                    </View>
                                    <View style={{ width: '100%', position: 'relative', zIndex: 0, height: 250, marginTop: 40}}>
                                        <Image source={slideBGLeft} style={{width: '100%', height: 250, resizeMode: 'contain',  position: 'absolute', right: 0, top: 0, zIndex: 10}} />
                                        <Text style={[styles.largeText, {right: 30}]}>02</Text>
                                        <Slideshow leftt={0} rightt={10}
                                                   dataSource={[
                                                       { url:'http://placeimg.com/640/480/any' },
                                                       { url:'http://placeimg.com/640/480/any' },
                                                       { url:'http://placeimg.com/640/480/any' }
                                                   ]} arrowSize={0} containerStyle={{paddingBottom: -30,  position: 'absolute', left: 5, top: 0, zIndex: 5}} height={250}  />
                                    </View>

                                    <ScrollView style={{ transform: [
                                        { scaleX: -1},
                                        // {  rotate: '180deg'},

                                    ],}}
                                                horizontal={true} showsHorizontalScrollIndicator={false}
                                    >
                                        <View style={styles.navitemContainer2}>
                                            <View>
                                                <TouchableOpacity onPress={() => this.handlePress('home')}>
                                                    <View style={styles.navContainer2}>
                                                        <Text style={[styles.text, {color: 'white', fontSize: 16}]}>14</Text>
                                                        <Text style={[styles.text, {color: 'white', paddingTop: 3}]}>ملیارد تومان</Text>
                                                    </View>
                                                </TouchableOpacity>
                                                <Text style={[styles.text, {paddingTop: 5, color: 'black', fontSize: 9, textAlign: 'center'}]}>فروش بیمه سال 97</Text>
                                            </View>
                                            <View>
                                                <TouchableOpacity onPress={() => this.handlePress('home')}>
                                                    <View style={styles.navContainer2}>
                                                        <Text style={[styles.text, {color: 'white', fontSize: 16}]}>14</Text>
                                                        <Text style={[styles.text, {color: 'white', paddingTop: 3}]}>ملیارد تومان</Text>
                                                    </View>
                                                </TouchableOpacity>
                                                <Text style={[styles.text, {paddingTop: 5, color: 'black', fontSize: 9}]}>فروش بیمه سال 97</Text>
                                            </View>
                                        </View>
                                    </ScrollView>
                                </View>
                                :
                                <View>
                                    {/*<LinearGradient start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']} style={styles.header}>*/}
                                    {/*<View style={styles.imageRow}>*/}
                                    {/*<View style={styles.profile}>*/}
                                    {/*/!*<View style={styles.iconContainer}>*!/*/}
                                    {/*/!*<Icon name="user" size={20} />*!/*/}
                                    {/*/!*</View>*!/*/}
                                    {/*/!*<Text style={styles.name}>{user !== null ? user.fname : ''} {user !== null ? user.lname : ''}</Text>*!/*/}
                                    {/*</View>*/}
                                    {/*<TouchableOpacity onPress={() => Actions.drawerOpen()}>*/}
                                    {/*<Icon name="menu" size={30} color="white" style={{paddingRight: 15}} />*/}
                                    {/*</TouchableOpacity>*/}
                                    {/*</View>*/}
                                    {/*<Image style={{position: 'absolute', top: 8, left: '39%', width: 50, height: 50, resizeMode: 'stretch'}}*/}
                                    {/*source={require('../../assets/headerLogo2.png')}/>*/}
                                    {/*</LinearGradient>*/}
                                        {/*<BuyHeader />*/}
                                    <View style={styles.body2}>
                                        <View style={styles.row}>
                                            <View style={styles.item}>
                                                <View
                                                    style={styles.imageContainer}>
                                                    <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, fire: true, pageTitle: 'بیمه آتش سوزی'})} >
                                                        <View>
                                                            <Image source={bime5} style={styles.bodyImage}  />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={styles.label}>آتش سوزی</Text>
                                            </View>
                                            <View style={styles.item}>
                                                <View
                                                    style={styles.imageContainer}>
                                                    <TouchableOpacity onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, bodyBime: true, pageTitle: 'بیمه بدنه خودرو'})} >
                                                        <View>
                                                            <Image source={bime1} style={styles.bodyImage} />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={styles.label}>بدنه</Text>
                                            </View>
                                            <View style={styles.item}>
                                                <View
                                                    style={styles.imageContainer}>
                                                    <TouchableOpacity onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true,motor: false, pageTitle: 'بیمه شخص ثالث'})} >
                                                        <View>
                                                            <Image source={bime2} style={styles.bodyImage} />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={styles.label}>شخص ثالث</Text>
                                            </View>
                                        </View>
                                        <View style={styles.row}>
                                            <View style={styles.item}>
                                                <View
                                                    style={styles.imageContainer}>
                                                    {/*<TouchableOpacity  onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, individual: true, pageTitle: 'بیمه درمان انفرادی'})} >*/}
                                                    <TouchableOpacity  onPress={()=> this.setState({modalVisible2: true})} >
                                                        <View>
                                                            <Image source={bime7} style={styles.bodyImage} />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={styles.label}>درمانی </Text>
                                            </View>
                                            <View style={styles.item}>
                                                <View
                                                    style={styles.imageContainer}>
                                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, medical: true, pageTitle: 'بیمه مسئولیت پزشکی'})} >*/}
                                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                                        <View>
                                                            <Image source={bime4} style={styles.bodyImage} />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={styles.label}>پزشکی</Text>
                                            </View>
                                            <View style={styles.item}>
                                                <View
                                                    style={styles.imageContainer}>
                                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, age: true, pageTitle: 'بیمه عمر'})} >*/}
                                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                                        <View>
                                                            <Image source={bime6} style={styles.bodyImage} />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={styles.label}>عمر</Text>
                                            </View>
                                        </View>
                                        <View style={styles.row}>
                                            <View style={styles.item}>
                                                <View
                                                    style={styles.imageContainer}>
                                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, travel: true, pageTitle: 'بیمه مسافرتی'})} >*/}
                                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                                        <View>
                                                            <Image source={bime3} style={styles.bodyImage} />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={styles.label}>مسافرتی</Text>
                                            </View>
                                            <View style={styles.item}>
                                                <View
                                                    style={styles.imageContainer}>
                                                    {/*<TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, earth: true,  pageTitle: 'بیمه زلزله'})} >*/}
                                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                                        <View>
                                                            <Image source={bime10} style={[styles.bodyImage, {width: 25}]} />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={styles.label}>زلزله</Text>
                                            </View>
                                            <View style={styles.item}>
                                                <View
                                                    style={styles.imageContainer}>
                                                    {/*<TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >*/}
                                                    <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                                        <View>
                                                            <Image source={mass} style={[styles.bodyImage, {width: 30}]} />
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                                <Text style={styles.label}>مسئولیت</Text>
                                            </View>
                                        </View>
                                        {
                                            this.state.loading ?
                                                <View style={{paddingTop: 30}}>
                                                    <ActivityIndicator
                                                        size={30}
                                                        animating={true}
                                                        color='rgb(39, 133, 228)'
                                                    />
                                                </View>
                                                :
                                                <View>
                                                    <Text style={styles.bodyLabel}>آخرین مطالب و مقالات</Text>
                                                    <View style={{width: '100%'}}>
                                                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ transform: [{ scaleX: -1}]}}>
                                                            <View style={styles.slideContainers}>
                                                                {
                                                                    this.state.spacialPosts ?
                                                                        this.state.spacialPosts.map((item) => {return <SlideItems item={item} key={item.id}  />}): null
                                                                }

                                                            </View>
                                                        </ScrollView>
                                                    </View>
                                                </View>
                                        }

                                        <AlertView
                                            closeModal={(title) => this.closeModal(title)}
                                            modalVisible={this.state.modalVisible}
                                            onChange={() => this.setState({modalVisible: false})}
                                            title={(this.state.wifi ? 'لطفا وضعیت وصل بودن به اینترنت را بررسی کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                                        />
                                        <SoonModal
                                            closeModal={(title) => this.closeModal2(title)}
                                            onChange={(visible) => this.setState({modalVisible2: false})}
                                            modalVisible={this.state.modalVisible2}
                                        />
                                    </View>
                                </View>
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            title={this.state.signed ?  'لطفا برای مشاهده این بخش ابتدا ثبت نام کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                        />
                    </View>
                </ScrollView>
                <FooterMenu active="profile" openDrawer={this.props.openDrawer} />
                <LinearGradient
                    start={{x: 0, y: 1}} end={{x: 1, y: 1}} colors={['rgb(0, 114, 255)', 'rgb(0, 128, 255)', 'rgb(0, 142, 255)']}>
                    <View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />
                </LinearGradient>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(Mainpage);
